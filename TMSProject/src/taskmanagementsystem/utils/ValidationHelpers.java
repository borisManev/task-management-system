package taskmanagementsystem.utils;

import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationHelpers {

    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Expected: %d, Received: %d. Invalid number of arguments!";

    public static void validateIntRange(int valueToValidate, int min, int max, String errorMessage) {
        if (valueToValidate < min || valueToValidate > max) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static void validateStringRange(String stringToValidate, int min, int max, String errorHead) {
        validateIntRange(stringToValidate.length(), min, max,
                String.format("%s should be between %d and %d symbols!",
                        errorHead, min, max));
    }

    public static <T> void validateNotNull(T t) {
        if (t == null) {
            throw new IllegalArgumentException("Null passed as argument!");
        }
    }

    public static void validateArgumentsCount(List<String> list, int expectedArgumentsCount) {
        if (list.size() < expectedArgumentsCount || list.size() > expectedArgumentsCount) {
            throw new InvalidUserInputException(String.format(INVALID_NUMBER_OF_ARGUMENTS, expectedArgumentsCount, list.size()));
        }
    }

    public static void validatePattern(String value, String pattern, String message) {
        Pattern patternToMatch = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = patternToMatch.matcher(value);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(message);
        }
    }
}
