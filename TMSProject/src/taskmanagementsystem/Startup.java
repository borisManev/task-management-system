package taskmanagementsystem;

import taskmanagementsystem.core.TSMEngineImpl;

public class Startup {

    public static void main(String[] args) {
        TSMEngineImpl engine = new TSMEngineImpl();
        engine.start();
    }
}
// test input/output can find in README.md

