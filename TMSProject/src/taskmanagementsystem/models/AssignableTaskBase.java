package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.task.AssignableTask;
import taskmanagementsystem.models.enums.Priority;

public abstract class AssignableTaskBase extends TaskBase implements AssignableTask {

    Priority priority;
    String assigneeName;
    boolean hasAssignee;

    public AssignableTaskBase(int id, String title, String description, Priority priority) {
        super(id, title, description);
        setPriority(priority);
        setAssigneeName("Not assigned yet.");
        hasAssignee = false;
    }

    @Override
    public void assignPerson(String personName) {
        if (hasAssignee) {
            throw new IllegalArgumentException(String.format("Already assigned to %s!", getAssigneeName()));
        }
        hasAssignee = true;
        assigneeName = personName;
    }

    @Override
    public void unassignPerson() {
        if (!hasAssignee) {
            throw new IllegalArgumentException("Task is not assigned, cannot unassign!");
        }
        hasAssignee = false;
    }

    @Override
    public boolean hasAssignee() {
        return hasAssignee;
    }

    public void setPriority(Priority priority) {
        if (priority == null) {
            throw new IllegalArgumentException("Null passed as parameter!");
        }
        this.priority = priority;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public String getAssigneeName() {
        if (!hasAssignee) {
            return "#No assignee#";
        }
        return assigneeName;
    }

    @Override
    protected String additionalInfo() {
        return String.format("Priority: %s, Assignee: %s", getPriority(), getAssigneeName());
    }
}
