package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.history.History;
import taskmanagementsystem.models.contracts.task.Comment;
import taskmanagementsystem.models.contracts.task.Task;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public abstract class TaskBase implements Task {
    private static final String STRING_REGEX_PATTERN = "^[A-Za-z0-9]+$";
    private static final String TASK_PATTERN_ERR = "Task title contains invalid symbols!";
    public static final int TITLE_MIN_LENGTH = 10;
    public static final int TITLE_MAX_LENGTH = 50;
    public static final int DESCRIPTION_MIN_LENGTH = 10;
    public static final int DESCRIPTION_MAX_LENGTH = 500;
    public static final String TASK_TITLE_ERROR = "Task title";
    public static final String TASK_DESCRIPTION_ERROR = "Task description";
    public static final String SPACER_MODELS = "----------";
    private final int id;
    private String title;
    private String description;
    private final List<Comment> comments;
    private final History history;

    public TaskBase(int id, String title, String description) {
        this.id = id;
        setTitle(title);
        setDescription(description);
        comments = new ArrayList<>();
        history = new HistoryImpl();
    }

    private void setTitle(String title) {
        ValidationHelpers.validatePattern(title, STRING_REGEX_PATTERN, TASK_PATTERN_ERR);
        ValidationHelpers.validateStringRange(title, TITLE_MIN_LENGTH, TITLE_MAX_LENGTH, TASK_TITLE_ERROR);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelpers.validateStringRange(description, DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH, TASK_DESCRIPTION_ERROR);
        this.description = description;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public History getHistory() {
        return new HistoryImpl(history);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public String toString() {
        return print() + System.lineSeparator() +
                getCommentsFormatted() +
                getHistory().toString().trim() + System.lineSeparator();
    }

    private String getCommentsFormatted() {
        StringBuilder sb = new StringBuilder();
        sb.append(SPACER_MODELS).append(System.lineSeparator());
        if (comments.isEmpty()) {
            sb.append("#No Comments#").append(System.lineSeparator());
        } else {
            sb.append("Comments:").append(System.lineSeparator());
            for (Comment comment : comments) {
                sb.append(comment.print()).append(System.lineSeparator());
            }
        }
        sb.append(SPACER_MODELS).append(System.lineSeparator());
        return sb.toString();
    }

    @Override
    public String print() {
        return String.format(SPACER_MODELS + System.lineSeparator() + "ID: %d, Title: %s" + System.lineSeparator() +
                        "Description: %s" + System.lineSeparator()
                        + additionalInfo() + System.lineSeparator() + SPACER_MODELS,
                getId(), getTitle(), getDescription());
    }

    @Override
    public void addActivity(Activity activity) {
        history.addActivity(activity);
    }

    //used to get the specific things from bug/story/task and print them
    protected abstract String additionalInfo();
}
