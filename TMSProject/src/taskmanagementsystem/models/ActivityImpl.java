package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.history.Activity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ActivityImpl implements Activity {

    String content;
    LocalDateTime localDate;

    public ActivityImpl(String content) {
        setContent(content);
        setLocalDate(LocalDateTime.now());
    }

    private void setContent(String content) {
        this.content = content;
    }

    private void setLocalDate(LocalDateTime localDate) {
        this.localDate = localDate;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public LocalDateTime getTimestamp() {
        return localDate;
    }

    public String getTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatDateTime = getTimestamp().format(formatter);

        return formatDateTime;
    }

    @Override
    public String print() {
        return String.format("Content: %s Date: %s%n", getContent(), getTime());
    }

    @Override
    public String toString() {
        return "Activity: " + content + ", time: " + getTime() + System.lineSeparator();
    }
}
