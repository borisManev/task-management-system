package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.task.Bug;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Severity;
import taskmanagementsystem.models.enums.StatusBug;
import taskmanagementsystem.utils.ParsingHelper;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends AssignableTaskBase implements Bug {

    public static final String INVALID_SEVERITY = "Severity cannot be empty!";
    public static final String INVALID_STEPS = "Steps cannot be empty!";
    public static final String INVALID_PRIORITY = "Priority must be one of Low, Medium, High!";
    private Severity severity;
    private List<String> stepsToReproduce;
    private StatusBug status;

    public BugImpl(int id, String title, String description, List<String> stepsToReproduce, Priority priority, Severity severity) {
        super(id, title, description, priority);
        setSeverity(severity);
        setStepsToReproduce(stepsToReproduce);
        status = StatusBug.ACTIVE;
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    public void setStepsToReproduce(List<String> stepsToReproduce) {
        if (stepsToReproduce.isEmpty()) {
            throw new IllegalArgumentException(String.format(INVALID_STEPS));
        }

        this.stepsToReproduce = stepsToReproduce;
    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    @Override
    public StatusBug getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = ParsingHelper.tryParseEnum(status, StatusBug.class,
                "%s is not a valid status for bug!");
    }

    @Override
    public void setSeverity(Severity newSeverity) {
        if (newSeverity == null) {
            throw new IllegalArgumentException(String.format(INVALID_SEVERITY));
        }
        this.severity = newSeverity;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected String additionalInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.additionalInfo()).append(System.lineSeparator());
        sb.append("Severity: ").append(getSeverity());
        sb.append(", Status: ").append(getStatus());
        sb.append(System.lineSeparator());
        if (stepsToReproduce.size() > 0) {
            sb.append("Steps to reproduce: ");
            int line = 1;
            for (String c : getStepsToReproduce()) {
                sb.append(System.lineSeparator());
                sb.append(line).append(". ");
                line++;
                sb.append(c);
            }
        }
        return sb.toString();
    }

    @Override
    public String getStatusAsString() {
        return status.toString();
    }
}
