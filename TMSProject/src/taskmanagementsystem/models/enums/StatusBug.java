package taskmanagementsystem.models.enums;

public enum StatusBug {
    ACTIVE,
    FIXED
}
