package taskmanagementsystem.models.enums;

public enum Severity {
    MINOR,
    MAJOR,
    CRITICAL
}
