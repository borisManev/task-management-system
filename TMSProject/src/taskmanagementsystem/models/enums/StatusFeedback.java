package taskmanagementsystem.models.enums;

public enum StatusFeedback {
    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE
}
