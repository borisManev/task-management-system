package taskmanagementsystem.models.enums;

public enum StatusStory {

    NOTDONE,
    INPROGRESS,
    DONE
}
