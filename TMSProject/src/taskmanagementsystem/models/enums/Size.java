package taskmanagementsystem.models.enums;

public enum Size {
    SMALL,
    MEDIUM,
    LARGE
}
