package taskmanagementsystem.models.enums;

public enum Priority {
    LOW,
    MEDIUM,
    HIGH
}
