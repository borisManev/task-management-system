package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.Board;
import taskmanagementsystem.models.contracts.Person;
import taskmanagementsystem.models.contracts.Team;
import taskmanagementsystem.models.contracts.task.Task;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TeamImpl implements Team {
    // can validate team name with validation regex  as in  StoryImpl or PersonImpl

    public static final int NAME_LEN_MIN = 5;
    public static final int NAME_LEN_MAX = 15;

    private static final String NAME_REGEX_PATTERN = "^[A-Za-z0-9]+$";
    private static final String NAME_PATTERN_ERR = "Team name contains invalid symbols!";

    private String name;
    private final List<Person> members;
    private final List<Board> boards;

    public TeamImpl(String name) {
        setName(name);
        this.members = new ArrayList<>();
        this.boards = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Person> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    public void setName(String name) {
        ValidationHelpers.validatePattern(name, NAME_REGEX_PATTERN, NAME_PATTERN_ERR);
        ValidationHelpers.validateStringRange(name, NAME_LEN_MIN, NAME_LEN_MAX, "Team name");
        this.name = name;
    }

    @Override
    public void addMember(Person person) {
        if (person == null) {
            throw new IllegalArgumentException("Null passed as argument.");
        }

        members.add(person);
    }

    @Override
    public void addBoard(Board board) {
        if (board == null) {
            throw new IllegalArgumentException("Null passed as argument.");
        }

        boards.add(board);
    }

    @Override
    public String print() {
        return String.format("Team name: %s", getName());
    }

    @Override
    public List<Task> getTasks() {
        return getBoards().stream().flatMap(board -> board.getTasks().stream()).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return print();
    }
}


