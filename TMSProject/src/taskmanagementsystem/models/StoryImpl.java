package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.task.Story;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Size;
import taskmanagementsystem.models.enums.StatusStory;
import taskmanagementsystem.utils.ParsingHelper;

public class StoryImpl extends AssignableTaskBase implements Story {

    private Size size;
    private StatusStory status;

    public StoryImpl(int id, String title, String description, Priority priority, Size size) {
        super(id, title, description, priority);
        setSize(size);
        this.status = StatusStory.NOTDONE;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public StatusStory getStatus() {
        return status;
    }

    @Override
    public void setStatus(StatusStory status) {
        this.status = status;
    }

    @Override
    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public void setStatus(String status) {
        this.status = ParsingHelper.tryParseEnum(status, StatusStory.class,
                "%s is not a valid status for story!");
    }

    @Override
    public String getStatusAsString() {
        return getStatus().toString();
    }
}
