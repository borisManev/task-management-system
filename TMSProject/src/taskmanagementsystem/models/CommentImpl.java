package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.task.Comment;

public class CommentImpl implements Comment {
    //boris
    private final String message;
    private final String author;

    public CommentImpl(String message, String author) {
        this.message = message;
        this.author = author;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public String print() {
        return String.format("Content: %s, Author: %s", getMessage(), getAuthor());
    }


}

