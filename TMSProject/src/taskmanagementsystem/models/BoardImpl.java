package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.Board;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.history.History;
import taskmanagementsystem.models.contracts.task.Task;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class BoardImpl implements Board {

    public static final int BOARD_NAME_MIN_LENGTH = 5;
    public static final int BOARD_NAME_MAX_LENGTH = 10;
    private String boardName;
    private final List<Task> tasks;

    public BoardImpl(String boardName) {
        setBoardName(boardName);
        tasks = new ArrayList<>();
    }

    private void setBoardName(String boardName) {
        ValidationHelpers.validateStringRange(boardName, BOARD_NAME_MIN_LENGTH, BOARD_NAME_MAX_LENGTH, "Board name");
        this.boardName = boardName;
    }

    @Override
    public String getName() {
        return boardName;
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    @Override
    public void addTask(Task task) {
        tasks.add(task);
    }

    @Override
    public History getHistory() {
        List<Activity> list = tasks
                .stream()
                .flatMap(task -> task.getHistory().getActivities().stream())
                .collect(Collectors.toList());

        return new HistoryImpl(list);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(SPACER_MODELS)
                .append(System.lineSeparator())
                .append(String.format("Board: %s", getName()))
                .append(System.lineSeparator());

        if (getTasks().isEmpty()) {
            stringBuilder.append("#No tasks#")
                    .append(System.lineSeparator());
        } else {
            for (Task task : tasks) {
                stringBuilder.append(task)
                        .append(System.lineSeparator());
            }
        }
        stringBuilder.append(SPACER_MODELS);
        return stringBuilder.toString();
    }

    @Override
    public String print() {
        return this.toString();
    }
}
