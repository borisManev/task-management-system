package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.Person;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.history.History;
import taskmanagementsystem.models.contracts.task.Task;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class PersonImpl implements Person {

    public static final int MIN_NAME_LENGTH = 5;
    public static final int MAX_NAME_LENGTH = 10;
    private static final String NAME_REGEX_PATTERN = "^[A-Za-z0-9]+$";
    private static final String NAME_PATTERN_ERR = "Person name contains invalid symbols!";
    String name;
    List<Task> tasks;
    List<Activity> history;

    public PersonImpl(String name) {
        setName(name);
        tasks = new ArrayList<>();
        history = new ArrayList<>();
    }

    private void setName(String name) {
        ValidationHelpers.validatePattern(name, NAME_REGEX_PATTERN, NAME_PATTERN_ERR);
        ValidationHelpers.validateStringRange(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH, "Person name");
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    public void addTasks(Task task) {
        tasks.add(task);
    }

    @Override
    public History getHistory() {
        return new HistoryImpl(new ArrayList<>(history));
    }

    @Override
    public void addActivity(Activity activity) {
        history.add(activity);
    }

    public List<Activity> getActivity() {
        return new ArrayList<>(history);
    }

    @Override
    public String print() {
        return "member: " + getName() + System.lineSeparator();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        print();
        if (tasks.size() == 0) {
            sb.append("#No tasks#").append(System.lineSeparator());
        } else {
            for (Task task : tasks) {
                sb.append(task.toString()).append(System.lineSeparator());
            }
        }

        if (history.size() == 0) {
            sb.append("#No history#");
        } else {
            for (Activity history : history) {
                sb.append(history.toString());
            }
        }

        return sb.toString();
    }
}
