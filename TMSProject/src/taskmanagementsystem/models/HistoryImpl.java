package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.history.History;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class HistoryImpl implements History {

    List<Activity> activityList;

    public HistoryImpl() {
        activityList = new ArrayList<>();
    }

    public HistoryImpl(History history) {
        ValidationHelpers.validateNotNull(history);
        activityList = new ArrayList<>(history.getActivities());
    }

    public HistoryImpl(List<Activity> listOfActivities) {
        ValidationHelpers.validateNotNull(listOfActivities);
        activityList = new ArrayList<>(listOfActivities);
    }

    @Override
    public List<Activity> getActivities() {
        return new ArrayList<>(activityList);
    }

    @Override
    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(SPACER_MODELS).append(System.lineSeparator());

        if (activityList.isEmpty()) {
            stringBuilder.append("#No History#").append(System.lineSeparator());
        } else {
            stringBuilder.append("History:").append(System.lineSeparator());
            for (Activity activity : activityList) {
                stringBuilder.append(activity.toString());
            }
        }
        stringBuilder.append(SPACER_MODELS).append(System.lineSeparator());
        return stringBuilder.toString();
    }

    @Override
    public String print() {
        return this.toString();
    }
}
