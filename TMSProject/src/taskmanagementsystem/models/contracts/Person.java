package taskmanagementsystem.models.contracts;

import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.history.History;
import taskmanagementsystem.models.contracts.task.Task;

import java.util.List;

public interface Person {

    String getName();

    List<Task> getTasks();

    History getHistory();

    void addActivity(Activity activity);

    String print();

    List<Activity> getActivity();
}
