package taskmanagementsystem.models.contracts;

import taskmanagementsystem.models.contracts.history.History;
import taskmanagementsystem.models.contracts.task.Task;

import java.util.List;

public interface Board {

    String getName();

    List<Task> getTasks();

    void addTask(Task task);

    History getHistory();

    String print();
}
