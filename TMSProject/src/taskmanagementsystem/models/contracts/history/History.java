package taskmanagementsystem.models.contracts.history;

import java.util.List;

public interface History {

    List<Activity> getActivities();

    void addActivity(Activity activity);

    String print();
}
