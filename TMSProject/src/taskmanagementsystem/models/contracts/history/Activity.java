package taskmanagementsystem.models.contracts.history;

import java.time.LocalDateTime;

public interface Activity {

    String getContent();

    LocalDateTime getTimestamp();

    String print();
}
