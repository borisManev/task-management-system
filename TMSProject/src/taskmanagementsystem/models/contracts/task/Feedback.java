package taskmanagementsystem.models.contracts.task;

import taskmanagementsystem.models.enums.StatusFeedback;

public interface Feedback extends Task {

    int getRating();

    StatusFeedback getStatus();

    void setStatus(StatusFeedback status);

    void setRating(int newRating);
}
