package taskmanagementsystem.models.contracts.task;

import taskmanagementsystem.models.enums.Size;
import taskmanagementsystem.models.enums.StatusStory;

public interface Story extends AssignableTask {

    Size getSize();

    StatusStory getStatus();

    void setStatus(StatusStory status);

    void setSize(Size newSize);
}
