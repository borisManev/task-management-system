package taskmanagementsystem.models.contracts.task;

import taskmanagementsystem.models.enums.Severity;
import taskmanagementsystem.models.enums.StatusBug;

import java.util.List;

public interface Bug extends AssignableTask {

    List<String> getStepsToReproduce();

    Severity getSeverity();

    StatusBug getStatus();

    void setStatus(String status);

    void setSeverity(Severity newSeverity);
}
