package taskmanagementsystem.models.contracts.task;

import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.history.History;

import java.util.List;

public interface Task {

    int getId();

    String getTitle();

    String getDescription();

    List<Comment> getComments();

    void addComment(Comment comment);

    void addActivity(Activity activity);

    History getHistory();

    String print();

    void setStatus(String status);

    boolean hasAssignee();

    String getStatusAsString();
}
