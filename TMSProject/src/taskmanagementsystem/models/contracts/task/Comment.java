package taskmanagementsystem.models.contracts.task;

public interface Comment {

    String getMessage();

    String getAuthor();

    String print();
}
