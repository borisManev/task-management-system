package taskmanagementsystem.models.contracts.task;

import taskmanagementsystem.models.enums.Priority;

public interface AssignableTask extends Task {

    Priority getPriority();

    String getAssigneeName();

    void setPriority(Priority newPriority);

    void assignPerson(String personName);

    void unassignPerson();
}
