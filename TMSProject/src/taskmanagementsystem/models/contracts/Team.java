package taskmanagementsystem.models.contracts;

import taskmanagementsystem.models.contracts.task.Task;

import java.util.List;

public interface Team {

    String getName();

    List<Person> getMembers();

    List<Board> getBoards();

    void addMember(Person person);

    void addBoard(Board board);

    String print();

    List<Task> getTasks();
}
