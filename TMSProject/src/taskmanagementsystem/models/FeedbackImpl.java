package taskmanagementsystem.models;

import taskmanagementsystem.models.contracts.task.Feedback;
import taskmanagementsystem.models.enums.StatusFeedback;
import taskmanagementsystem.utils.ParsingHelper;
import taskmanagementsystem.utils.ValidationHelpers;

public class FeedbackImpl extends TaskBase implements Feedback {

    public static final int RATING_MIN_VAL = 1;
    public static final int RATING_MAX_VAL = 10;
    public static final String ERROR_MESSAGE =
            String.format("Rating has to be between %d and %d.", RATING_MIN_VAL, RATING_MAX_VAL);

    private int rating;
    private StatusFeedback status;

    public FeedbackImpl(int id, String title, String description) {
        super(id, title, description);
        rating = RATING_MIN_VAL;
        this.status = StatusFeedback.NEW;
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public StatusFeedback getStatus() {
        return status;
    }

    @Override
    public String getStatusAsString() {
        return status.toString();
    }

    @Override
    public void setStatus(StatusFeedback status) {
        this.status = status;
    }

    @Override
    public void setRating(int newRating) {
        ValidationHelpers.validateIntRange(newRating, RATING_MIN_VAL, RATING_MAX_VAL, ERROR_MESSAGE);
        this.rating = newRating;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected String additionalInfo() {
        return String.format("Rating: %d, Status: %s", getRating(), getStatus());
    }

    @Override
    public void setStatus(String status) {
        this.status = ParsingHelper.tryParseEnum(status, StatusFeedback.class,
                "%s is not a valid status for feedback!");
    }

    @Override
    public boolean hasAssignee() {
        return false;
    }
}
