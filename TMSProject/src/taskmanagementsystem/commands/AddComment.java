package taskmanagementsystem.commands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.CommentImpl;
import taskmanagementsystem.utils.ParsingHelper;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class AddComment implements Command {

    public static final int EXPECTED_PARAMETERS = 3;
    private static final String COMMENT_ADDED = "Comment added to task: %d by: %s.";
    public static final String AUTHOR_NOT_REGISTERED = "Author is not registered, need to be create person first!";
    private final TSMRepository repository;

    public AddComment(TSMRepository repository) {
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS);
        int commentID = ParsingHelper.tryParseInteger(parameters.get(0), "task ID");
        String commentMessage = (parameters.get(1));
        String commentAuthor = (parameters.get(2));

        return addComment(commentID, commentMessage, commentAuthor);
    }

    private String addComment(int ID, String Message, String Author) {
        if (!repository.personExists(Author)) {
            throw new InvalidUserInputException(AUTHOR_NOT_REGISTERED);
        }
        repository.findTaskById(ID).addComment(new CommentImpl(Message, Author));
        ActivityImpl activity = new ActivityImpl(String.format(COMMENT_ADDED, ID, Author));
        repository.findTaskById(ID).addActivity(activity);
        repository.findPersonByName(Author).addActivity(activity);
        return String.format(COMMENT_ADDED, ID, Author);
    }
    //stanimir
}
