package taskmanagementsystem.commands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.task.Task;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;

import static taskmanagementsystem.commands.ListAllTasksWithActiveAssignee.LIST_SEPARATORS;
import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class ListAllTasks implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS_FILTER = 2;
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS_SORT = 1;
    public static final String INVALID_COMMAND = "This is an invalid command";
    public static final String NO_TASKS_TO_FILTER = "There are not tasks to filter";
    private final TSMRepository tsmRepository;

    public ListAllTasks(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String result;
        if (parameters.isEmpty()) {
            result = listAllTasks();
        } else if ("filter".equalsIgnoreCase(parameters.get(0))) {
            ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS_FILTER);
            String title = parameters.get(1);
            result = filterByTitle(title);
        } else if ("sort".equalsIgnoreCase(parameters.get(0))) {
            ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS_SORT);
            result = sortByTitle();
        } else {
            throw new InvalidUserInputException(INVALID_COMMAND);
        }
        return LIST_SEPARATORS + System.lineSeparator() + "List All Tasks :" + System.lineSeparator() + result.trim() + System.lineSeparator() + LIST_SEPARATORS;
    }

    private String listAllTasks() {
        if (tsmRepository.getTasks().isEmpty()) return NO_TASKS_TO_FILTER;
        return tsmRepository
                .getTasks()
                .stream()
                .map(Task::print)
                .reduce("", (a, b) -> a + b + System.lineSeparator());
    }

    private String sortByTitle() {
        if (tsmRepository.getTasks().isEmpty()) return NO_TASKS_TO_FILTER;
        return tsmRepository
                .getTasks()
                .stream()
                .sorted(Comparator.comparing(Task::getTitle))
                .map(Task::print)
                .reduce("", (a, b) -> a + b + System.lineSeparator());
    }

    private String filterByTitle(String title) {
        if (tsmRepository.getTasks().isEmpty()) {
            return NO_TASKS_TO_FILTER;
        }
        StringBuilder sb = new StringBuilder();

        for (Task task : tsmRepository.getTasks()) {
            if (task.getTitle().equals(title)) {
                sb.append(String.format("ID: %s, ", task.getId()));
                sb.append(String.format("Title: %s", task.getTitle()));
                sb.append(System.lineSeparator());
                sb.append(String.format("Description: %s", task.getDescription()));
                sb.append(System.lineSeparator());
                sb.append(String.format("Status: %s", task.getStatusAsString()));
                sb.append(System.lineSeparator());
                sb.append(SPACER_MODELS).append(System.lineSeparator());
            }
        }
        if (sb.toString().isEmpty()) {
            throw new InvalidUserInputException(String.format("No task with title: %s - cannot filter!", title));
        }
        return sb.toString();
    }
    //boris
}
