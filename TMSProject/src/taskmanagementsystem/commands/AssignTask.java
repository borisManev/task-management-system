package taskmanagementsystem.commands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.contracts.Person;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.task.AssignableTask;
import taskmanagementsystem.models.contracts.task.Task;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

import static taskmanagementsystem.commands.AddPerson.PERSON_DOES_NOT_EXIST;

public class AssignTask implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String TASK_ASSIGNED_SUCCESS_MESSAGE = "Task: %d assigned to %s.";
    private final TSMRepository tsmRepository;

    public AssignTask(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String personName = parameters.get(0).trim();
        int taskID = Integer.parseInt(parameters.get(1).trim());

        if (!tsmRepository.personExists(personName)) {
            throw new InvalidUserInputException(String.format(PERSON_DOES_NOT_EXIST, personName));
        }
        if (!tsmRepository.taskExists(taskID)) {
            throw new NumberFormatException(String.format("Task with id: %d does not exist!", taskID));
        }
        Person person = tsmRepository.findPersonByName(personName);
        Task task = tsmRepository.findTaskById(taskID);
        tsmRepository.checkTaskAndMemberOneTeam(person, task);

        AssignableTask assignableTask = tsmRepository.getAssignableTasks()
                .stream()
                .filter(t -> t.getId() == taskID)
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException("Task is not assignable!"));
        if (assignableTask.hasAssignee()) {
            throw new InvalidUserInputException("Task cannot be assigned as it has an assignee already!");
        }
        assignableTask.assignPerson(person.getName());

        Activity activity = new ActivityImpl(String.format("Task with id: %d - assigned to %s.", taskID, personName));
        assignableTask.addActivity(activity);

        Activity activity1 = new ActivityImpl(String.format("Assigned task with id:%d.", assignableTask.getId()));

        tsmRepository.findPersonByName(personName).addActivity(activity1);
        return String.format(TASK_ASSIGNED_SUCCESS_MESSAGE, taskID, personName);
    }
    //boris
}
