package taskmanagementsystem.commands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.commands.enums.FiltrationOptions;
import taskmanagementsystem.commands.enums.SortingOptions;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.task.AssignableTask;
import taskmanagementsystem.models.contracts.task.Task;
import taskmanagementsystem.utils.ParsingHelper;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListAllTasksWithActiveAssignee implements Command {

    public static final String LIST_SEPARATORS = "==========";
    public static final String LIST_INFO = "List tasks with active Assignee:";
    TSMRepository tsmRepository;
    FiltrationOptions filter;
    SortingOptions sorting;

    public ListAllTasksWithActiveAssignee(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() < 2 || parameters.size() > 4) {
            throw new InvalidUserInputException("Command options not specified");
        }
        filter = ParsingHelper.tryParseEnum(parameters.get(0).trim(), FiltrationOptions.class,
                "%s are not valid filter options.");
        sorting = ParsingHelper.tryParseEnum(parameters.get(1).trim(), SortingOptions.class,
                "%s are not valid sorting options.");
        List<Task> tasks = tsmRepository.getTasks()
                .stream()
                .filter(Task::hasAssignee)
                .collect(Collectors.toList());

        tasks = filter(parameters, tasks);
        if (tasks.isEmpty() && !tsmRepository.getTasks().isEmpty()) {
            throw new InvalidUserInputException("No tasks matched the filter");
        }
        tasks = sort(tasks);
        return listToString(tasks);
    }

    private List<Task> filter(List<String> parameters, List<Task> tasks) {
        switch (filter) {
            case NO_FILTER: {
                ValidationHelpers.validateArgumentsCount(parameters, 2);
                return tasks;
            }
            case FILTER_BY_ASSIGNEE: {
                parametersNotEmptyValidation(parameters);
                ValidationHelpers.validateArgumentsCount(parameters, 3);
                String assigneeName = parameters.get(2).trim();
                if (!tsmRepository.personExists(assigneeName)) {
                    throw new InvalidUserInputException("Filter parameter wrong. No such Assignee!");
                }
                List<Task> filtered = tasks
                        .stream()
                        .map(task -> (AssignableTask) task)
                        .filter(task -> task.getAssigneeName().equals(assigneeName))
                        .collect(Collectors.toList());

                return filtered;
            }
            case FILTER_BY_STATUS: {
                ValidationHelpers.validateArgumentsCount(parameters, 3);
                parametersNotEmptyValidation(parameters);
                String status = parameters.get(2).trim();

                return tasks
                        .stream()
                        .filter(task -> task.getStatusAsString().equalsIgnoreCase(status))
                        .collect(Collectors.toList());
            }
            case FILTER_BY_ASSIGNEE_AND_STATUS: {
                ValidationHelpers.validateArgumentsCount(parameters, 4);
                parametersNotEmptyValidation(parameters);
                String assigneeName = parameters.get(2).trim();

                parametersNotEmptyValidation(parameters);
                String status = parameters.get(3).trim();

                return tasks
                        .stream()
                        .map(task -> (AssignableTask) task)
                        .filter(task -> task.getAssigneeName().equals(assigneeName))
                        .filter(task -> task.getStatusAsString().equalsIgnoreCase(status))
                        .collect(Collectors.toList());
            }
            default: {
                throw new InvalidUserInputException(
                        "ListAllAssignedTasks command can only filter by assignee and/or status.");
            }
        }
    }

    private List<Task> sort(List<Task> tasks) {
        switch (sorting) {
            case NO_SORT: {
                return tasks;
            }
            case SORT_BY_TITLE: {
                return tasks
                        .stream()
                        .sorted(Comparator.comparing(Task::getTitle))
                        .collect(Collectors.toList());
            }
            default: {
                throw new InvalidUserInputException("ListAllAssignedTasks command can only sort by title");
            }
        }
    }

    private String listToString(List<Task> tasks) {
        String result = tasks
                .stream()
                .flatMap(task -> Stream.of(task.toString()))
                .reduce("", String::concat);

        return LIST_SEPARATORS + System.lineSeparator() + LIST_INFO + System.lineSeparator() + result + LIST_SEPARATORS;
    }

    private void parametersNotEmptyValidation(List<String> parameters) {
        if (parameters.isEmpty()) {
            throw new InvalidUserInputException("Invalid number of arguments.");
        }
    }
}
