package taskmanagementsystem.commands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.contracts.Person;
import taskmanagementsystem.models.contracts.Team;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class AddPerson implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String PERSON_DOES_NOT_EXIST = "Person with name: %s does not exist!";
    TSMRepository tsmRepository;

    public AddPerson(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String teamName = parameters.get(0).trim();
        String personName = parameters.get(1).trim();

        if (!tsmRepository.teamExists(teamName)) {
            throw new InvalidUserInputException("Team does not exist!");
        }
        if (!tsmRepository.personExists(personName)) {
            throw new InvalidUserInputException(String.format(PERSON_DOES_NOT_EXIST, personName));
        }
        Person person = tsmRepository.findPersonByName(personName);
        Team team = tsmRepository.findTeamByName(teamName);
        if (team.getMembers()
                .stream()
                .anyMatch(p -> p.getName().equals(personName))) {
            throw new InvalidUserInputException(String.format("%s is already in %s!", personName, teamName));
        }
        team.addMember(person);
        Activity activity = new ActivityImpl(String.format("Joined team with name: %s", teamName));
        person.addActivity(activity);

        return String.format("%s added - to team: %s successfully.", personName, teamName);
    }
}
