package taskmanagementsystem.commands.enums;

public enum FiltrationOptions {
    NO_FILTER,
    FILTER_BY_TITLE,
    FILTER_BY_ASSIGNEE,
    FILTER_BY_STATUS,
    FILTER_BY_ASSIGNEE_AND_STATUS
}
