package taskmanagementsystem.commands.enums;

public enum SortingOptions {
    NO_SORT,
    SORT_BY_TITLE,
    SORT_BY_PRIORITY,
    SORT_BY_SEVERITY,
    SORT_BY_SIZE,
    SORT_BY_RATING
}
