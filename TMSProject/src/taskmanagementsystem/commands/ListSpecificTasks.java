package taskmanagementsystem.commands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ListSpecificTasks implements Command {
    public static final String LIST_SEPARATORS = "==========";
    public static final int EXPECTED_PARAMETERS_COUNT = 1;
    private final TSMRepository repository;

    public ListSpecificTasks(TSMRepository repository) {
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS_COUNT);
        String targetTitle = parameters.get(0);
        return listSpecificTasks(targetTitle);
    }

    private String listSpecificTasks(String targetTitle) {
        String result = repository.getTasks().stream()
                .filter(task -> task.getTitle().equals(targetTitle))
                .map(task -> task.print())
                .reduce("", String::concat);
        if (result.isBlank()) {
            result = "#No Task to List with title:" + targetTitle + System.lineSeparator();
        }
        return LIST_SEPARATORS + result + LIST_SEPARATORS;
    }
    //stanimir
}
