package taskmanagementsystem.commands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.task.AssignableTask;
import taskmanagementsystem.utils.ParsingHelper;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class UnassignTask implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    TSMRepository tsmRepository;

    public UnassignTask(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int id = ParsingHelper.tryParseInteger(parameters.get(0).trim(), "Id");
        String personName = parameters.get(1).trim();

        if (!tsmRepository.taskExists(id)) {
            throw new InvalidUserInputException("Task does not exist!");
        }
        if (!tsmRepository.personExists(personName)) {
            throw new InvalidUserInputException("Member does not exist!");
        }
        AssignableTask assignableTask = tsmRepository.getAssignableTasks()
                .stream()
                .filter(t -> t.getId() == id)
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException("Task is not assignable!"));

        if (!assignableTask.hasAssignee()) {
            throw new InvalidUserInputException("Task cannot be unassigned as it has no assignee!");
        }

        String assigneeName = assignableTask.getAssigneeName();
        assignableTask.unassignPerson();
        Activity activity = new ActivityImpl(String.format("Assignee %s was unassigned by %s.", assigneeName, personName));
        assignableTask.addActivity(activity);
        Activity activity1 = new ActivityImpl(String.format("Unassigned task with id:%d.", assignableTask.getId()));
        tsmRepository.findPersonByName(personName).addActivity(activity1);

        return String.format("Unassigned person from task with id:%d successfully.", assignableTask.getId());
    }
}
