package taskmanagementsystem.commands.changecommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.task.Feedback;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangeRating implements Command {

    public static final int EXPECTED_PARAMETERS_COUNT = 3;
    public static final String RATING_CHANGED = "Rating of task: %d, changed to %s by: %s.";
    private final TSMRepository repository;

    public ChangeRating(TSMRepository repository) {
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS_COUNT);

        int ID = Integer.parseInt(parameters.get(0));
        int rating = Integer.parseInt(parameters.get(1));
        String author = parameters.get(2);

        return changeRating(ID, rating, author);
    }

    private String changeRating(int ID, int rating, String author) {
        if (!repository.personExists(author)) {
            throw new InvalidUserInputException(author + " is not registered person!");
        }

        Feedback feedback = repository.findFeedbackById(ID);
        feedback.setRating(rating);
        Activity activity = new ActivityImpl(String.format(RATING_CHANGED, ID, rating, author));
        feedback.addActivity(activity);
        repository.findPersonByName(author).addActivity(activity);
        return String.format(RATING_CHANGED, ID, rating, author);
    }

    //stanimir
}
