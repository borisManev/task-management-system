package taskmanagementsystem.commands.changecommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.task.Bug;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangeStatus implements Command {

    public static final int EXPECTED_PARAMETERS_COUNT = 3;
    public static final String STATUS_CHANGED = "Status of task: %d, changed to %s by: %s.";
    private final TSMRepository repository;

    public ChangeStatus(TSMRepository repository) {
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS_COUNT);

        int ID = Integer.parseInt(parameters.get(0));
        String status = parameters.get(1);
        String author = parameters.get(2);

        return changeStatus(ID, status, author);
    }

    private String changeStatus(int ID, String status, String author) {
        if (!repository.personExists(author)) {
            throw new InvalidUserInputException("%s is not registered person!");
        }
        Bug bug = repository.findBugById(ID);
        if (bug.getAssigneeName().equals(author) && repository.findBugById(ID).hasAssignee()) {
            throw new InvalidUserInputException(String.format("Task with ID: %d is assigned to %s and cannot by modified by %s!"
                    , ID, repository.findBugById(ID).getAssigneeName(), author));
        }
        bug.setStatus(status);
        Activity activity = new ActivityImpl(String.format(STATUS_CHANGED, ID, status, author));
        repository.findTaskById(ID).addActivity(activity);
        repository.findPersonByName(author).addActivity(activity);
        return String.format(STATUS_CHANGED, ID, status, author);
    }
//stanimir
}
