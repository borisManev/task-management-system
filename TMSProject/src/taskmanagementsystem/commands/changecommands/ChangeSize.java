package taskmanagementsystem.commands.changecommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.task.Story;
import taskmanagementsystem.models.enums.Size;
import taskmanagementsystem.utils.ParsingHelper;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangeSize implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    TSMRepository tsmRepository;

    public ChangeSize(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int id = ParsingHelper.tryParseInteger(parameters.get(0).trim(), "Id");
        Size size = ParsingHelper.tryParseEnum(parameters.get(1).trim(), Size.class,
                "%s is not a valid task type!");
        String personName = parameters.get(2).trim();

        if (!tsmRepository.taskExists(id)) {
            throw new InvalidUserInputException("Task does not exist!");
        }
        if (!tsmRepository.personExists(personName)) {
            throw new InvalidUserInputException("Person does not exist!");
        }

        Story story = tsmRepository.getStories()
                .stream()
                .filter(t -> t.getId() == id)
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException("Task has no size, cannot be changed!"));

        Size initialSize = story.getSize();

        if (initialSize.equals(size)) {
            throw new InvalidUserInputException(String.format("Size is already %s, cannot be change!", size));
        }

        story.setSize(size);

        Activity activity = new ActivityImpl(String.format("Size was changed from %s to %s by %s.",
                initialSize, size, personName));
        story.addActivity(activity);

        Activity activity1 = new ActivityImpl(String.format("Changed size of task with id:%d from %s to %s.",
                story.getId(), initialSize, size));
        tsmRepository.findPersonByName(personName).addActivity(activity1);

        return String.format("Size for task with id:%d changed from %s to %s successfully.",
                story.getId(), initialSize, size);
    }
}
