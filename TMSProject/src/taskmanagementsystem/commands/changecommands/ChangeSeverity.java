package taskmanagementsystem.commands.changecommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.task.Bug;
import taskmanagementsystem.models.enums.Severity;
import taskmanagementsystem.utils.ParsingHelper;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangeSeverity implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    public static final String SEVERITY_CHANGED_SUCCESSFULLY = "Severity of bug with id:%d changed from %s to %s, by: %s successfully.";

    private final TSMRepository tsmRepository;

    public ChangeSeverity(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        Severity severity = ParsingHelper.
                tryParseEnum(parameters.get(1).trim(), Severity.class, "%s is not a valid priority!");
        int taskID = ParsingHelper.tryParseInteger(parameters.get(0).trim(), "Id");
        String personName = parameters.get(2).trim();

        return changeSeverity(severity, taskID, personName);
    }

    private String changeSeverity(Severity severity, int taskID, String name) {
        Bug bug = tsmRepository.getBugs()
                .stream()
                .filter(t -> t.getId() == taskID)
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException("Task has no severity!"));

        Severity initialSeverity = bug.getSeverity();

        if (initialSeverity.equals(severity)) {
            throw new InvalidUserInputException(String.format("Severity is already %s!", severity));
        }
        bug.setSeverity(severity);

        String active = (String.format(SEVERITY_CHANGED_SUCCESSFULLY,
                taskID, initialSeverity, severity, name));
        Activity activity = new ActivityImpl(active);
        bug.addActivity(activity);
        tsmRepository.findPersonByName(name).addActivity(activity);

        return active;
    }
    //boris
}

