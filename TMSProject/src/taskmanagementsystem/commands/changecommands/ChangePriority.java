package taskmanagementsystem.commands.changecommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.task.AssignableTask;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.utils.ParsingHelper;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangePriority implements Command {

    //ChangePriority taskId priority Author
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    public static final String PRIORITY_CHANGED = "Priority of task with id:%d changed from %s to %s successfully by: %s.";
    TSMRepository tsmRepository;

    public ChangePriority(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int id = ParsingHelper.tryParseInteger(parameters.get(0).trim(), "Id");
        Priority priority = ParsingHelper.tryParseEnum(parameters.get(1).trim(), Priority.class,
                "%s is not a valid task type!");
        String personName = parameters.get(2).trim();

        if (!tsmRepository.taskExists(id)) {
            throw new InvalidUserInputException("Task does not exist!");
        }
        if (!tsmRepository.personExists(personName)) {
            throw new InvalidUserInputException("Person does not exist!");
        }

        AssignableTask assignableTask = tsmRepository.getAssignableTasks()
                .stream()
                .filter(t -> t.getId() == id)
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException("Task has no priority."));

        Priority initialPriority = assignableTask.getPriority();

        if (initialPriority.equals(priority)) {
            throw new InvalidUserInputException(String.format("Priority is already %s.", priority));
        }
        assignableTask.setPriority(priority);

        Activity activity = new ActivityImpl(String.format(PRIORITY_CHANGED,
                assignableTask.getId(), initialPriority, priority, personName));
        assignableTask.addActivity(activity);

        Activity activity1 = new ActivityImpl(String.format("Changed size of task with id:%d from %s to %s.",
                assignableTask.getId(), initialPriority, priority));
        tsmRepository.findPersonByName(personName).addActivity(activity1);

        return String.format(PRIORITY_CHANGED,
                assignableTask.getId(), initialPriority, priority, personName);
    }
}
