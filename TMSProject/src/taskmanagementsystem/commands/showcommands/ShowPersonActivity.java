package taskmanagementsystem.commands.showcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

import static taskmanagementsystem.commands.ListAllTasksWithActiveAssignee.LIST_SEPARATORS;
import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class ShowPersonActivity implements Command {
    public static final int EXPECTED_PARAMETERS_COUNT = 1;
    private final TSMRepository repository;

    public ShowPersonActivity(TSMRepository repository) {
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS_COUNT);
        String targetTitle = parameters.get(0);
        return showPersonActivity(targetTitle);
    }

    private String showPersonActivity(String targetTitle) {
        StringBuilder sb = new StringBuilder();
        sb.append(LIST_SEPARATORS)
                .append(System.lineSeparator())
                .append(String.format("Show person activity: %s", targetTitle))
                .append(System.lineSeparator())
                .append(SPACER_MODELS)
                .append(System.lineSeparator());
        String result = repository.findPersonByName(targetTitle).getActivity().stream()
                .map(activity -> activity.print())
                .reduce("", String::concat);
        if (result.isBlank()) {
            sb.append("No Activity").append(System.lineSeparator());
        } else {
            sb.append(result.trim());
        }
        sb.append(System.lineSeparator())
                .append(SPACER_MODELS)
                .append(System.lineSeparator())
                .append(LIST_SEPARATORS);
        return sb.toString();
    }
    //stanimir
}
