package taskmanagementsystem.commands.showcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.models.contracts.Team;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

import static taskmanagementsystem.commands.showcommands.ShowAllTeamMembers.LISTING_SPACER;
import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class ShowAllTeams implements Command {
    public static final int EXPECTED_ARGUMENTS_COUNT = 0;
    private final TSMRepository tsmRepository;

    public ShowAllTeams(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_ARGUMENTS_COUNT);

        StringBuilder sb = new StringBuilder();
        sb.append(LISTING_SPACER).append(System.lineSeparator())
                .append("List all Teams :").append(System.lineSeparator());
        sb.append(SPACER_MODELS).append(System.lineSeparator());
        if (tsmRepository.getTeams().isEmpty()) {
            sb.append("NO REGISTERED TEAMS").append(System.lineSeparator());
        } else {
            for (Team team : tsmRepository.getTeams()) {
                sb.append(team.print()).append(System.lineSeparator());
            }
            sb.append(SPACER_MODELS).append(System.lineSeparator());
            sb.append(LISTING_SPACER);
        }
        return sb.toString();
    }
}
