package taskmanagementsystem.commands.showcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.Team;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Stream;

public class ShowAllTeamBoards implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    TSMRepository tsmRepository;

    public ShowAllTeamBoards(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String teamName = parameters.get(0).trim();
        if (!tsmRepository.teamExists(teamName)) {
            throw new InvalidUserInputException("Team name is invalid.");
        }
        return showBoards(tsmRepository.findTeamByName(teamName));
    }

    private String showBoards(Team team) {
        if (team.getBoards().isEmpty()) {
            return "#No boards#";
        }
        return team.getBoards()
                .stream()
                .flatMap(board -> Stream.of(board.toString()))
                .reduce((s1, s2) -> s1 + System.lineSeparator() + s2)
                .get();
    }
}
