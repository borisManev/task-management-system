package taskmanagementsystem.commands.showcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class ShowAllTeamMembers implements Command {
    public static final int EXPECTED_PARAMETERS = 1;
    public static final String LISTING_SPACER = "==========";
    private final TSMRepository repository;

    public ShowAllTeamMembers(TSMRepository repository) {
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS);
        String targetTeam = parameters.get(0);
        return showAllTeamMembers(targetTeam);
    }

    private String showAllTeamMembers(String targetTeam) {
        StringBuilder sb = new StringBuilder();
        sb.append(LISTING_SPACER).append(System.lineSeparator())
                .append(String.format("List all members of team: %s", targetTeam))
                .append(System.lineSeparator());
        sb.append(SPACER_MODELS).append(System.lineSeparator());
        if (repository.findTeamByName(targetTeam).getMembers().size() == 0) {
            sb.append("# No members #" + System.lineSeparator());
        } else {
            sb.append(repository.findTeamByName(targetTeam).getMembers().stream()
                    .map(person -> person.print())
                    .reduce("", String::concat));
        }
        sb.append(SPACER_MODELS).append(System.lineSeparator());
        sb.append(LISTING_SPACER);
        return sb.toString();
    }
//stanimir
}
