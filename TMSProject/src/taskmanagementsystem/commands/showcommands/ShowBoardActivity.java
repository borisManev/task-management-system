package taskmanagementsystem.commands.showcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

import static taskmanagementsystem.commands.ListAllTasksWithActiveAssignee.LIST_SEPARATORS;
import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class ShowBoardActivity implements Command {

    //georgi
    public static final int EXPECTED_PARAMETERS_COUNT = 2;
    private final TSMRepository repository;

    public ShowBoardActivity(TSMRepository repository) {
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS_COUNT);

        String teamName = parameters.get(0).trim();
        String boardName = parameters.get(1).trim();

        if (!repository.boardExistsInTeam(teamName, boardName)) {
            throw new InvalidUserInputException("Board does not exist.");
        }
        return showBoardActivity(teamName, boardName);
    }

    private String showBoardActivity(String teamName, String boardName) {
        StringBuilder sb = new StringBuilder();
        sb.append(LIST_SEPARATORS)
                .append(System.lineSeparator())
                .append(String.format("Show activity of board: %s from team: %s", boardName, teamName))
                .append(System.lineSeparator())
                .append(SPACER_MODELS)
                .append(System.lineSeparator());
        if (repository.findBoardByTeamAndBoardName(teamName, boardName).getHistory().getActivities().isEmpty()) {
            sb.append("#No Activity#");
        }
        String result = repository.findBoardByTeamAndBoardName(teamName, boardName).getHistory().getActivities().stream()
                .map(Activity::print)
                .reduce("", String::concat);
        sb.append(result.trim());
        sb.append(System.lineSeparator())
                .append(SPACER_MODELS)
                .append(System.lineSeparator())
                .append(LIST_SEPARATORS);
        return sb.toString();
    }
}
