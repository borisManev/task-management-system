package taskmanagementsystem.commands.showcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

import static taskmanagementsystem.commands.ListAllTasksWithActiveAssignee.LIST_SEPARATORS;
import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class ShowTeamActivity implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final TSMRepository tsmRepository;

    public ShowTeamActivity(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String teamName = parameters.get(0);
        if (teamName.isEmpty() || !tsmRepository.teamExists(teamName)) {
            throw new InvalidUserInputException("Team name is invalid.");
        }
        return showActivity(teamName);
    }

    private String showActivity(String team) {
        StringBuilder sb = new StringBuilder();
        sb.append(LIST_SEPARATORS)
                .append(System.lineSeparator())
                .append(String.format("Show team activity: %s", team))
                .append(System.lineSeparator());
        if (tsmRepository.findTeamByName(team).getBoards().isEmpty()) {
            sb.append(SPACER_MODELS)
                    .append(System.lineSeparator())
                    .append("#No activities#");
        }
        sb.append(tsmRepository.findTeamByName(team).getBoards().stream()
                .map(board -> board.getHistory().toString()).reduce("", String::concat));
        sb.append(LIST_SEPARATORS);
        return sb.toString();
    }
    //boris
}

