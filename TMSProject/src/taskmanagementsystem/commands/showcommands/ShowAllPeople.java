package taskmanagementsystem.commands.showcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

import static taskmanagementsystem.commands.showcommands.ShowAllTeamMembers.LISTING_SPACER;

public class ShowAllPeople implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;
    private final TSMRepository tsmRepository;

    public ShowAllPeople(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        return showAllPeople();
    }

    private String showAllPeople() {
        StringBuilder sb = new StringBuilder();
        sb.append(LISTING_SPACER)
                .append(System.lineSeparator())
                .append("List of all Peoples:")
                .append(System.lineSeparator());

        if (tsmRepository.getPeople().isEmpty()) {
            sb.append("#No People#").append(System.lineSeparator());
        } else {
            tsmRepository.getPeople()
                    .stream()
                    .map(person -> person.toString())
                    .reduce((p1, p2) -> p1 + System.lineSeparator() + p2)
                    .get();
        }
        sb.append(LISTING_SPACER).append(System.lineSeparator());
        return sb.toString();
    }
    //boris
}
