package taskmanagementsystem.commands.createcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class CreateBoard implements Command {

    //boris
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String BOARD_CREATED_MESSAGE = "Board: %s - created successfully in team: %s.";
    public static final String TEAM_DOES_NOT_EXIST = "Team named %s does not exist!";
    public static final String BOARD_ALREADY_EXISTS = "Board named %s already exists!";
    private final TSMRepository tsmRepository;

    public CreateBoard(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String teamName = parameters.get(0).trim();
        String boardName = parameters.get(1).trim();

        return createBoard(boardName, teamName);
    }

    private String createBoard(String boardName, String teamName) {
        if (tsmRepository.teamExists(teamName)) {
            if (tsmRepository.findTeamByName(teamName).getBoards()
                    .stream()
                    .anyMatch(board -> board.getName().equals(boardName))) {
                throw new InvalidUserInputException(String.format(BOARD_ALREADY_EXISTS, boardName));
            }
            tsmRepository.createBoardInTeam(teamName, boardName);
            return String.format(BOARD_CREATED_MESSAGE, boardName, teamName);
        }
        throw new InvalidUserInputException(String.format(TEAM_DOES_NOT_EXIST, teamName));
    }
}
