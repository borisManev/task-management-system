package taskmanagementsystem.commands.createcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Severity;
import taskmanagementsystem.models.enums.Size;
import taskmanagementsystem.models.enums.TaskType;
import taskmanagementsystem.utils.ParsingHelper;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class CreateTask implements Command {

    private static final int EXPECTED_NUMBER_OF_BASE_ARGUMENTS = 5;
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS_BUG = 3 + EXPECTED_NUMBER_OF_BASE_ARGUMENTS;
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS_STORY = 2 + EXPECTED_NUMBER_OF_BASE_ARGUMENTS;
    TSMRepository tsmRepository;
    TaskType taskType;
    private String teamName;
    private String boardName;
    private String title;
    private String description;

    public CreateTask(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.isEmpty()) {
            throw new InvalidUserInputException("Invalid number of arguments.No arguments provided.");
        }
        taskType = ParsingHelper.tryParseEnum(parameters.get(0).trim(), TaskType.class,
                "%s is not a valid task type.");
        createTask(parameters, taskType);

        return String.format("Task type:%s - created successfully.", taskType.toString());
    }

    private void parseAndValidateBaseArguments(List<String> parameters) {
        teamName = parameters.get(1).trim();
        boardName = parameters.get(2).trim();
        title = parameters.get(3).trim();
        description = parameters.get(4).trim();

        if (teamName.isEmpty() || boardName.isEmpty() || title.isEmpty() || description.isEmpty()) {
            throw new InvalidUserInputException("Invalid arguments.");
        }
        if (!tsmRepository.boardExistsInTeam(teamName, boardName)) {
            throw new InvalidUserInputException("Invalid board to add the task to.");
        }
    }

    private void createTask(List<String> parameters, TaskType taskType) {
        int index = EXPECTED_NUMBER_OF_BASE_ARGUMENTS;

        switch (taskType) {
            case BUG: {
                ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS_BUG);
                parseAndValidateBaseArguments(parameters);

                List<String> stepsToReporoduce = parseStepsToReproduce(parameters.get(index).trim());
                Priority priority = ParsingHelper.tryParseEnum(parameters.get(index + 1).trim(), Priority.class,
                        "%s is not a valid priority.");
                Severity severity = ParsingHelper.tryParseEnum(parameters.get(index + 2).trim(), Severity.class,
                        "%s is not a valid severity.");

                tsmRepository.createBug(teamName, boardName, title, description, stepsToReporoduce, priority, severity);
                break;
            }
            case STORY: {
                ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS_STORY);
                parseAndValidateBaseArguments(parameters);

                Priority priority = ParsingHelper.tryParseEnum(parameters.get(index).trim(), Priority.class,
                        "%s is not a valid priority.");
                Size size = ParsingHelper.tryParseEnum(parameters.get(index + 1).trim(), Size.class,
                        "%s is not a valid size.");

                tsmRepository.createStory(teamName, boardName, title, description, priority, size);
                break;
            }
            case FEEDBACK: {
                ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_BASE_ARGUMENTS);
                parseAndValidateBaseArguments(parameters);

                tsmRepository.createFeedback(teamName, boardName, title, description);
                break;
            }
        }
    }

    private List<String> parseStepsToReproduce(String param) {
        if (param.isEmpty()) {
            throw new InvalidUserInputException("Invalid arguments.");
        }
        List<String> stepsToReproduce = List.of(param.split(("[1-9]+[.]")));

        return stepsToReproduce
                .stream()
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
    }
}