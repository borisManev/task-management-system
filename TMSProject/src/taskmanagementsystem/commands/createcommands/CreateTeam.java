package taskmanagementsystem.commands.createcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class CreateTeam implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String TEAM_CREATED_MESSAGE = "Team %s - created successfully.";
    private final TSMRepository tsmRepository;

    public CreateTeam(TSMRepository tsmRepository) {
        this.tsmRepository = tsmRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String teamName = parameters.get(0).trim();
        return createTeam(teamName);
    }

    private String createTeam(String teamName) {
        if (tsmRepository.teamExists(teamName)) {
            throw new InvalidUserInputException("Team with this name already exists!");
        }
        tsmRepository.createTeam(teamName);
        return String.format(TEAM_CREATED_MESSAGE, teamName);
    }
    //boris
}
