package taskmanagementsystem.commands.createcommands;

import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class CreatePerson implements Command {

    public static final int EXPECTED_PARAMETERS_COUNT = 1;
    public static final String CREATED_PERSON = "Created new person with name: %s.";
    private final TSMRepository repository;

    public CreatePerson(TSMRepository repository) {
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS_COUNT);
        return createPerson(parameters.get(0).trim());
    }

    private String createPerson(String name) {
        repository.createPerson(name);
        return String.format(CREATED_PERSON, name);
    }
    //stanimir
}