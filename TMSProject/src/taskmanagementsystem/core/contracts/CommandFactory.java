package taskmanagementsystem.core.contracts;

import taskmanagementsystem.commands.contracts.Command;

public interface CommandFactory {

    Command createCommandFromCommandName(String commandName, TSMRepository repository);
}
