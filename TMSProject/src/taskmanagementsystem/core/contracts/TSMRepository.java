package taskmanagementsystem.core.contracts;

import taskmanagementsystem.models.contracts.Board;
import taskmanagementsystem.models.contracts.Person;
import taskmanagementsystem.models.contracts.Team;
import taskmanagementsystem.models.contracts.task.*;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Severity;
import taskmanagementsystem.models.enums.Size;

import java.util.List;

public interface TSMRepository {

    List<Team> getTeams();

    List<Person> getPeople();

    List<Board> getBoards();

    List<Task> getTasks();

    List<AssignableTask> getAssignableTasks();

    List<Bug> getBugs();

    List<Story> getStories();

    List<Feedback> getFeedbacks();

    Team findTeamByName(String name);

    Person findPersonByName(String name);

    Board findBoardByTeamAndBoardName(String teamName, String boardName);

    Task findTaskById(int id);

    void createTeam(String teamName);

    void createPerson(String personName);

    void createBoardInTeam(String teamName, String boardName);

    void createBug(String teamName, String boardName,
                   String title, String description, List<String> listOfStepsToReproduce,
                   Priority priority, Severity severity);

    void createStory(String teamName, String boardName,
                     String title, String description, Priority priority, Size size);

    void createFeedback(String teamName, String boardName,
                        String title, String description);

    boolean teamExists(String teamName);

    boolean personExists(String personName);

    boolean boardExistsInTeam(String teamName, String boardName);

    boolean taskExists(int taskId);

    void checkTaskAndMemberOneTeam(Person member, Task task);

    Feedback findFeedbackById(int id);

    Bug findBugById(int id);
}
