package taskmanagementsystem.core.contracts;

public interface Engine {

    void start();
}
