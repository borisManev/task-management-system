package taskmanagementsystem.core;

import taskmanagementsystem.commands.*;
import taskmanagementsystem.commands.changecommands.*;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.commands.createcommands.CreateBoard;
import taskmanagementsystem.commands.createcommands.CreatePerson;
import taskmanagementsystem.commands.createcommands.CreateTask;
import taskmanagementsystem.commands.createcommands.CreateTeam;
import taskmanagementsystem.commands.enums.CommandType;
import taskmanagementsystem.commands.showcommands.*;
import taskmanagementsystem.core.contracts.CommandFactory;
import taskmanagementsystem.core.contracts.TSMRepository;

import static taskmanagementsystem.utils.ParsingHelper.tryParseEnum;

public class CommandFactoryImpl implements CommandFactory {
    private static final String COMMAND_NOT_SUPPORTED_MESSAGE = "Command %s is not supported.";

    @Override
    public Command createCommandFromCommandName(String commandTypeValue, TSMRepository repository) {
        CommandType commandType = tryParseEnum(commandTypeValue, CommandType.class, String.format(COMMAND_NOT_SUPPORTED_MESSAGE, commandTypeValue));

        switch (commandType) {
            case ADDCOMMENT:
                return new AddComment(repository);
            case ADDPERSON:
                return new AddPerson(repository);
            case ASSIGNTASK:
                return new AssignTask(repository);
            case CHANGEPRIORITY:
                return new ChangePriority(repository);
            case CHANGERATING:
                return new ChangeRating(repository);
            case CHANGESEVERITY:
                return new ChangeSeverity(repository);
            case CHANGESIZE:
                return new ChangeSize(repository);
            case CHANGESTATUS:
                return new ChangeStatus(repository);
            case CREATEBOARD:
                return new CreateBoard(repository);
            case CREATEPERSON:
                return new CreatePerson(repository);
            case CREATETASK:
                return new CreateTask(repository);
            case CREATETEAM:
                return new CreateTeam(repository);
            case LISTALLTASKS:
                return new ListAllTasks(repository);
            case LISTALLTASKSWITHACTIVEASSIGNEE:
                return new ListAllTasksWithActiveAssignee(repository);
            case LISTSPECIFICTASKS:
                return new ListSpecificTasks(repository);
            case SHOWALLPEOPLE:
                return new ShowAllPeople(repository);
            case SHOWALLTEAMBOARDS:
                return new ShowAllTeamBoards(repository);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembers(repository);
            case SHOWALLTEAMS:
                return new ShowAllTeams(repository);
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivity(repository);
            case SHOWPERSONACTIVITY:
                return new ShowPersonActivity(repository);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivity(repository);
            case UNASSIGNTASK:
                return new UnassignTask(repository);
            default:
                throw new UnsupportedOperationException(String.format(COMMAND_NOT_SUPPORTED_MESSAGE, commandTypeValue));
        }
    }
}
