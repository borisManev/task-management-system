package taskmanagementsystem.core;

import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.DuplicateEntityException;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.*;
import taskmanagementsystem.models.contracts.Board;
import taskmanagementsystem.models.contracts.Person;
import taskmanagementsystem.models.contracts.Team;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.task.*;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Severity;
import taskmanagementsystem.models.enums.Size;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TSMRepositoryImpl implements TSMRepository {
    public static final String MEMBER_TEAM_NOT_TASK_TEAM = "Member: %s is in team: %s, Task with id: %d is in team: %s, command not accepted!";
    private int nextId;
    List<Team> teams;
    List<Person> people;
    List<Bug> bugs;
    List<Story> stories;
    List<Feedback> feedbacks;

    public TSMRepositoryImpl() {
        nextId = 1;
        teams = new ArrayList<>();
        people = new ArrayList<>();
        bugs = new ArrayList<>();
        stories = new ArrayList<>();
        feedbacks = new ArrayList<>();
    }

    @Override
    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    @Override
    public List<Person> getPeople() {
        return new ArrayList<>(people);
    }

    @Override
    public List<Board> getBoards() {
        return teams
                .stream()
                .flatMap(team -> team.getBoards().stream())
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> getTasks() {
        return getBoards()
                .stream()
                .flatMap(board -> board.getTasks().stream())
                .collect(Collectors.toList());
    }

    @Override
    public List<AssignableTask> getAssignableTasks() {
        List<AssignableTask> list = new ArrayList<>();
        list.addAll(bugs);
        list.addAll(stories);
        return list;
    }

    @Override
    public List<Bug> getBugs() {
        return bugs;
    }

    @Override
    public List<Story> getStories() {
        return stories;
    }

    @Override
    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }

    @Override
    public Team findTeamByName(String name) {
        return teams
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException(
                        String.format("There is no team with name: %s", name)));
    }

    @Override
    public Person findPersonByName(String name) {
        return people
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException(
                        String.format("There is no person with name: %s", name)));
    }

    @Override
    public Board findBoardByTeamAndBoardName(String teamName, String boardName) {
        return findTeamByName(teamName).getBoards()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(boardName))
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException(
                        String.format("There is no board with name: %s in team: %s", boardName, teamName)));
    }

    @Override
    public Task findTaskById(int id) {
        return getTasks()
                .stream()
                .filter(u -> u.getId() == id)
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException(
                        String.format("There is no task with id: %s", id)));
    }

    @Override
    public void createTeam(String teamName) {
        if (teamExists(teamName)) {
            throw new DuplicateEntityException("Cannot create team with the same name.");
        }
        Team team = new TeamImpl(teamName);
        teams.add(team);
    }

    @Override
    public void createPerson(String personName) {
        if (personExists(personName)) {
            throw new DuplicateEntityException("Cannot create person with the same name!");
        }
        Person person = new PersonImpl(personName);
        people.add(person);

        Activity activity = new ActivityImpl(String.format(
                "Person with name:%s created.", personName));
        person.addActivity(activity);
    }

    @Override
    public void createBoardInTeam(String teamName, String boardName) {
        if (boardExistsInTeam(teamName, boardName)) {
            throw new DuplicateEntityException("Cannot create board with the same name in the team!");
        }
        Board board = new BoardImpl(boardName);
        findTeamByName(teamName).addBoard(board);
    }

    @Override
    public void createBug(String teamName, String boardName, String title, String description,
                          List<String> listOfStepsToReproduce, Priority priority, Severity severity) {

        Bug bug = new BugImpl(nextId++, title, description, listOfStepsToReproduce, priority, severity);
        findBoardByTeamAndBoardName(teamName, boardName).addTask(bug);

        bugs.add(bug);

        Activity activity = new ActivityImpl(String.format(
                "Bug with id:%d created.", nextId - 1));
        bug.addActivity(activity);
    }

    @Override
    public void createStory(String teamName, String boardName, String title, String description, Priority priority, Size size) {

        Story story = new StoryImpl(nextId++, title, description, priority, size);
        findBoardByTeamAndBoardName(teamName, boardName).addTask(story);

        stories.add(story);

        Activity activity = new ActivityImpl(String.format(
                "Story with id:%d created.", nextId - 1));
        story.addActivity(activity);
    }

    @Override
    public void createFeedback(String teamName, String boardName, String title, String description) {
        Feedback feedback = new FeedbackImpl(nextId++, title, description);
        findBoardByTeamAndBoardName(teamName, boardName).addTask(feedback);

        feedbacks.add(feedback);

        Activity activity = new ActivityImpl(String.format(
                "Feedback with id:%d created.", nextId - 1));
        feedback.addActivity(activity);
    }

    @Override
    public boolean teamExists(String teamName) {
        return teams
                .stream()
                .anyMatch(team -> team.getName().equals(teamName));
    }

    @Override
    public boolean personExists(String personName) {
        return people
                .stream()
                .anyMatch(person -> person.getName().equals(personName));
    }

    @Override
    public boolean boardExistsInTeam(String teamName, String boardName) {
        if (!teamExists(teamName)) {
            return false;
        }
        return findTeamByName(teamName).getBoards()
                .stream()
                .anyMatch(board -> board.getName().equals(boardName));
    }

    @Override
    public boolean taskExists(int taskId) {
        return getTasks()
                .stream()
                .anyMatch(task -> task.getId() == taskId);
    }

    public Team findTeamByMember(Person member) {
        return getTeams().stream().filter(team -> team.getMembers().contains(member)).findFirst()
                .orElseThrow(() -> new InvalidUserInputException(
                        String.format("No member with name: %s!", member.getName())));
    }

    public Team findTeamByTask(Task task) {
        return getTeams().stream().filter(team -> team.getTasks().contains(task)).findFirst()
                .orElseThrow(() -> new InvalidUserInputException(
                        String.format("No task with id: %d!", task.getId())));
    }

    public void checkTaskAndMemberOneTeam(Person member, Task task) {
        String teamNameMember = findTeamByMember(member).getName();
        String teamNameTask = findTeamByTask(task).getName();
        if (!teamNameTask.equals(teamNameMember)) {
            throw new InvalidUserInputException(String.format(MEMBER_TEAM_NOT_TASK_TEAM, member.getName(), teamNameMember, task.getId(), teamNameTask));
        }
    }

    public Feedback findFeedbackById(int id) {
        return getFeedbacks()
                .stream()
                .filter(f -> f.getId() == id)
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException(
                        String.format("No Feedback with id: %s!", id)));
    }

    public Bug findBugById(int id) {
        return getBugs()
                .stream()
                .filter(f -> f.getId() == id)
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException(
                        String.format("No Bug with id: %s!", id)));
    }
}
