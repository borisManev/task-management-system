package models;


import org.junit.jupiter.api.Test;
import taskmanagementsystem.models.StoryImpl;
import taskmanagementsystem.models.contracts.task.Story;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Size;
import taskmanagementsystem.models.enums.StatusStory;

import static Utils.TestUtils.VALID_DESCRIPTION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StoryImplTests {

    public static final int VALID_ID = 1;
    public static final String VALID_TITLE = "TitleTitle";
    public static final String INVALID_TITLE = "TitleTitl";

    @Test
    public void should_CreateStory_When_ValidValuesArePassed() {
        Story story = new StoryImpl(VALID_ID, VALID_TITLE, VALID_DESCRIPTION, Priority.HIGH, Size.LARGE);

        assertTrue(story instanceof Story);
    }

    @Test
    public void getStatus_Should_Return_CurrentStatus() {

        Story story = new StoryImpl(VALID_ID, VALID_TITLE, VALID_DESCRIPTION, Priority.HIGH, Size.LARGE);

        assertEquals(StatusStory.NOTDONE, story.getStatus());
    }

    @Test
    public void setStatus_Should_ChangeStatus_When_ArgumentsAreValid() {
        Story story = new StoryImpl(VALID_ID, VALID_TITLE, VALID_DESCRIPTION, Priority.HIGH, Size.LARGE);
        StatusStory difStatus = StatusStory.INPROGRESS;
        story.setStatus(difStatus);

        assertEquals(difStatus, story.getStatus());

    }

    @Test
    public void getStatusAsString_Should_Return_StringStatus() {

        Story story = new StoryImpl(VALID_ID, VALID_TITLE, VALID_DESCRIPTION, Priority.HIGH, Size.LARGE);

        assertEquals(StatusStory.NOTDONE.toString(), story.getStatusAsString());
    }


}
