package models;

//import org.junit.Test;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.models.BoardImpl;
import taskmanagementsystem.models.PersonImpl;
import taskmanagementsystem.models.TeamImpl;
import taskmanagementsystem.models.contracts.Board;
import taskmanagementsystem.models.contracts.Person;
import taskmanagementsystem.models.contracts.Team;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class TeamImplTests {

    @Test
    public void should_CreateTeam_When_ValidValuesArePassed() {
        Team team = new TeamImpl(VALID_TEAM_NAME);

        assertAll(
                () -> assertEquals(team.getName(), VALID_TEAM_NAME),
                () -> assertTrue(team.getBoards().isEmpty()),
                () -> assertTrue(team.getMembers().isEmpty())
        );
    }

    @Test
    public void constructor_Should_ThrowException_When_TeamNameInvalid() {
        assertThrows(IllegalArgumentException.class, () -> new TeamImpl(INVALID_TEAM_NAME));
    }

    @Test
    public void getMembers_Should_ReturnCopyOfCollection() {
        Team team = new TeamImpl(VALID_TEAM_NAME);
        Person person = new PersonImpl(VALID_PERSON);

        team.getMembers().add(person);

        assertEquals(0, team.getMembers().size());
    }

    @Test
    public void getBoards_Should_ReturnCopyOfCollection() {
        Team team = new TeamImpl(VALID_TEAM_NAME);
        Board board = new BoardImpl(VALID_BOARD_NAME);

        team.getBoards().add(board);

        assertEquals(0, team.getBoards().size());
    }

    @Test
    public void addMember_Should_AddMemberToCollection_When_ArgumentsAreValid() {
        Team team = new TeamImpl(VALID_TEAM_NAME);
        Person person = new PersonImpl(VALID_PERSON);

        team.addMember(person);

        assertEquals(1, team.getMembers().size());
        assertEquals(team.getMembers().get(0), person);
    }

    @Test
    public void addBoard_Should_AddBoardToCollection_When_ArgumentsAreValid() {
        Team team = new TeamImpl(VALID_TEAM_NAME);
        Board board = new BoardImpl(VALID_BOARD_NAME);

        team.addBoard(board);

        assertEquals(1, team.getBoards().size());
        assertEquals(team.getBoards().get(0), board);
    }

    @Test
    public void addMember_Should_ThrowException_When_ArgumentsAreInvalid() {
        Team team = new TeamImpl(VALID_TEAM_NAME);

        assertThrows(IllegalArgumentException.class, () -> team.addMember(null));
    }

    @Test
    public void addBoard_Should_ThrowException_When_ArgumentsAreInvalid() {
        Team team = new TeamImpl(VALID_TEAM_NAME);

        assertThrows(IllegalArgumentException.class, () -> team.addBoard(null));
    }

    @Test
    public void setName_Should_ThrowException_When_ArgumentsAreInvalid(){
        TeamImpl team = new TeamImpl(VALID_TEAM_NAME);

        assertThrows(IllegalArgumentException.class,()-> team.setName(INVALID_TEAM_NAME));
    }
}
