package models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.models.ActivityImpl;
import taskmanagementsystem.models.contracts.history.Activity;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ActivityImplTests {

    public static final String NEW_CONTENT = "New content";

    @Test
    public void constructor_Should_CreateActivity_When_ValidArgumentsArePassed() {
        Activity activity = new ActivityImpl(NEW_CONTENT);

        assertEquals(activity.getContent(), NEW_CONTENT);
    }

    @Test
    public void print_Should_PrintContent_When_ProductsAreNotEmpty() {
        ActivityImpl activity = new ActivityImpl(NEW_CONTENT);

        String expected = String.format("Content: %s Date: %s%n", activity.getContent(),
                activity.getTime());

        Assertions.assertEquals(expected, activity.print());
    }
}
