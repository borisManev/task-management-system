package models;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.BugImpl;
import taskmanagementsystem.models.contracts.task.Bug;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Severity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class BugImplTests {

    private final int validId = 3;
    private List<String> validStepsToReproduce;
    private Priority priority;
    private Severity severity;

    @BeforeEach
    public void beforeEach() {
        List<String> helper = List.of(VALID_STEPS_TO_REPRODUCE.split("[1-9]+[.]"));
        validStepsToReproduce = helper
                .stream()
                .map(String::trim)
                .collect(Collectors.toList());

        priority = Priority.valueOf(VALID_PRIORITY);
        severity = Severity.valueOf(VALID_SEVERITY);
    }

    @Test
    public void should_CreateBug_When_ValidArgumentsArePassed() {
        Bug bug = initializeValidBug();

        assertAll(
                () -> assertEquals(validId, bug.getId()),
                () -> assertEquals(VALID_TASK_TITLE, bug.getTitle()),
                () -> assertEquals(VALID_DESCRIPTION, bug.getDescription()),
                () -> assertEquals(validStepsToReproduce, bug.getStepsToReproduce()),
                () -> assertEquals(priority, bug.getPriority()),
                () -> assertEquals(severity, bug.getSeverity())
        );
    }

    @Test
    public void constructor_Should_ThrowException_When_TitleIsInvalid() {
        assertThrows(IllegalArgumentException.class, () -> new BugImpl(
                validId, INVALID_TASK_TITLE, VALID_DESCRIPTION, validStepsToReproduce, priority, severity));
    }

    @Test
    public void constructor_Should_ThrowException_When_DescriptionIsInvalid() {
        assertThrows(IllegalArgumentException.class, () -> new BugImpl(
                validId, VALID_TASK_TITLE, INVALID_DESCRIPTION, validStepsToReproduce, priority, severity));
    }

    @Test
    public void constructor_Should_ThrowException_When_StepsToReproduceIsEmpty() {
        List<String> list = new ArrayList<>();

        assertThrows(IllegalArgumentException.class,()-> new BugImpl(
                validId, VALID_TASK_TITLE, VALID_DESCRIPTION, list, priority, severity));

    }

    @Test
    public void constructor_Should_ThrowException_When_PriorityIsInvalid() {
        assertThrows(IllegalArgumentException.class,()-> new BugImpl(
                validId, VALID_TASK_TITLE, VALID_DESCRIPTION, validStepsToReproduce, null, severity));
    }

    @Test
    public void constructor_Should_ThrowException_When_SeverityIsInvalid() {
        assertThrows(IllegalArgumentException.class,()-> new BugImpl(
                validId, VALID_TASK_TITLE, VALID_DESCRIPTION, validStepsToReproduce, priority, null));
    }

    @Test
    public void getStepsToReproduce_Should_ReturnCopyOfCollection(){
        Bug bug = initializeValidBug();
        int initialSize = bug.getStepsToReproduce().size();

        bug.getStepsToReproduce().add("new step");

        assertEquals(initialSize,bug.getStepsToReproduce().size());
    }

    @Test
    public void setStepsToReproduce_Should_ThrowException_When_StepsToReproduceIsEmpty(){
        BugImpl bug = (BugImpl)initializeValidBug();
        List<String> list = new ArrayList<>();

        assertThrows(IllegalArgumentException.class,()-> bug.setStepsToReproduce(list));
    }

    @Test
    public void setStatus_Should_ThrowException_When_StatusIsInvalid(){
        Bug bug = initializeValidBug();
        assertThrows(InvalidUserInputException.class, ()->bug.setStatus("This is an invalid status"));
    }

    @Test
    public void setSeverity_Should_ThrowException_When_ArgumentsAreInvalid(){
        Bug bug = initializeValidBug();

        assertThrows(IllegalArgumentException.class, ()-> bug.setSeverity(null));
    }

    private Bug initializeValidBug(){
        return new BugImpl(validId, VALID_TASK_TITLE, VALID_DESCRIPTION, validStepsToReproduce, priority, severity);
    }
}
