package models;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.models.*;
import taskmanagementsystem.models.contracts.Board;
import taskmanagementsystem.models.contracts.Person;
import taskmanagementsystem.models.contracts.Team;
import taskmanagementsystem.models.contracts.history.Activity;
import taskmanagementsystem.models.contracts.history.History;
import taskmanagementsystem.models.contracts.task.Bug;
import taskmanagementsystem.models.contracts.task.Story;
import taskmanagementsystem.models.contracts.task.Task;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Size;
import taskmanagementsystem.models.enums.StatusStory;

import java.util.ArrayList;

import static Utils.TestUtils.*;
import static Utils.TestUtils.VALID_PERSON;
import static org.junit.jupiter.api.Assertions.*;

public class PersonImplTests {
    public static final String VALID_NAME = "TitleTitle";
    public static final String INVALID_NAME_LENGTH = "Tit";
    public static final String INVALID_NAME = "!!)(#@#(!";
    public static final int VALID_ID = 1;
    public static final String VALID_TITLE = "TitleTitle";
    public static final String NEW_CONTENT = "New content";

    @Test
    public void should_CreateStory_When_ValidValuesArePassed() {
        Person person = new PersonImpl(VALID_NAME);

        assertEquals(person.getName(), VALID_NAME);

    }

    @Test
    public void should_ThrowException_WhenLengthIs_OutOfBounds() {
        assertThrows(IllegalArgumentException.class, () -> new PersonImpl(INVALID_NAME_LENGTH));

    }

    @Test
    public void should_ThrowException_WhenNameIsInvalid() {
        assertThrows(IllegalArgumentException.class, () -> new PersonImpl(INVALID_NAME));

    }


    @Test
    public void getTasks_Should_ReturnCopyOfCollection() {
        Person person = new PersonImpl(VALID_NAME);
        Story story = new StoryImpl(VALID_ID, VALID_TITLE, VALID_DESCRIPTION, Priority.HIGH, Size.LARGE);
        person.getTasks().add(story);

        assertEquals(0, person.getTasks().size());
    }

    @Test
    public void getActivity_Should_ReturnCopyOfCollection() {
        Person person = new PersonImpl(VALID_NAME);
        Activity activity = new ActivityImpl(NEW_CONTENT);
        person.getHistory().addActivity(activity);

        assertEquals(0, person.getActivity().size());
    }
    @Test
    public void getHistory_Should_ReturnCopyOfCollection() {
        Person person = new PersonImpl(VALID_NAME);
        Activity activity = new ActivityImpl(NEW_CONTENT);
        person.getHistory().addActivity(activity);

        assertEquals(0, person.getHistory().getActivities().size());
    }

    @Test
    public void toString_Should_ReturnNoHistory_NoTask_WhenThereAreNone() {
        Person person = new PersonImpl(VALID_NAME);
        StringBuilder sb = new StringBuilder();

        if (person.getTasks().size() == 0) {
            sb.append("#No tasks#").append(System.lineSeparator());
        } else {
            for (Task task : person.getTasks()) {
                sb.append(task.toString()).append(System.lineSeparator());
            }
        }

        if (person.getHistory().getActivities().size() == 0) {
            sb.append("#No history#");
        } else {
            for (Activity history : person.getHistory().getActivities()) {
                sb.append(history.toString());
            }
        }

        Assertions.assertEquals(sb.toString(),person.toString());
    }

    @Test
    public void toString_Should_ReturnHistory_NoTask_WhenThereIsActivities() {
        Person person = new PersonImpl(VALID_NAME);
        StringBuilder sb = new StringBuilder();
        Activity activity = new ActivityImpl(NEW_CONTENT);
        person.getHistory().addActivity(activity);

        if (person.getTasks().size() == 0) {
            sb.append("#No tasks#").append(System.lineSeparator());
        } else {
            for (Task task : person.getTasks()) {
                sb.append(task.toString()).append(System.lineSeparator());
            }
        }

        if (person.getHistory().getActivities().size() == 0) {
            sb.append("#No history#");
        } else {
            for (Activity history : person.getHistory().getActivities()) {
                sb.append(history.toString());
            }
        }

        Assertions.assertEquals(sb.toString(),person.toString());
    }

}
