package models;

import org.junit.jupiter.api.Test;
import taskmanagementsystem.models.CommentImpl;
import taskmanagementsystem.models.contracts.task.Comment;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class CommentImplTests {

    @Test
    public void constructor_Should_CreateComment_When_ValidArgumentsArePassed(){

        Comment comment = new CommentImpl(VALID_COMMENT_CONTENT,VALID_PERSON);

        assertAll(
                ()-> assertEquals(comment.getMessage(), VALID_COMMENT_CONTENT),
                ()->assertEquals(comment.getAuthor(),VALID_PERSON)
        );
    }
}
