package commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.changecommands.ChangeStatus;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.task.Bug;

import java.util.Arrays;

import static Utils.TestUtils.*;

public class ChangeStatusTests {

    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        command = new ChangeStatus(repository);
        createTestPerson(repository);
        createTestTeam(repository);
        repository.findTeamByName(VALID_TEAM_NAME).addMember(repository.findPersonByName(VALID_PERSON));
        repository.createBoardInTeam(VALID_TEAM_NAME, VALID_BOARD_NAME);
    }

    @Test
    public void execute_Should_ThrowException_when_NotValidParameters() {
        createTestBug(repository);
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(VALID_BUG_STATUS)));
    }

    @Test
    public void execute_Should_ThrowException_when_NotValidStatus() {
        createTestBug(repository);
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("1", INVALID_BUG_STATUS, VALID_PERSON)));
    }

    @Test
    public void execute_Should_ThrowException_when_NotValidParam() {
        createTestBug(repository);
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(INVALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ChangeStatus_when_ValidParameters() {
        createTestBug(repository);
        command.execute(Arrays.asList("1", VALID_BUG_STATUS, VALID_PERSON));
        Bug bug = (Bug) repository.findTaskById(1);
        Assertions.assertEquals(VALID_BUG_STATUS, bug.getStatus().toString());
    }

    @Test
    public void execute_Should_ThrowException_when_NoSuchTask() {

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("1", VALID_BUG_STATUS)));
    }

    //boris
}
