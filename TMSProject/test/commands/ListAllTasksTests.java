package commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.ListAllTasks;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Severity;

import java.util.Arrays;
import java.util.List;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static taskmanagementsystem.commands.ListAllTasksWithActiveAssignee.LIST_SEPARATORS;
import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class ListAllTasksTests {

    public static final String FIRST_TITLE = "ddddddddddddd";
    public static final String SECOND_TITLE = "cccccccccccccc";
    public static final String THIRD_TITLE = "bbbbbbbbbbbbbbb";
    public static final String FOURTH_TITLE = "aaaaaaaaaaa";
    public static final String ALL_TASKS = "List All Tasks :";
    private Command command;
    private TSMRepository repository;

    public static final String FILTER = "FILTER";
    public static final String WRONG_FILTER = "FILTER_BY";

    public static final String SORT = "SORT";
    public static final String WRONG_SORT = "SORT_BY";


    @BeforeEach
    public void before() {
        repository = new TSMRepositoryImpl();
        createTestPerson(repository);
        createTestTeam(repository);
        createTestBoard(repository);

        repository.createBug(VALID_TEAM_NAME, VALID_BOARD_NAME, FIRST_TITLE, VALID_DESCRIPTION,
                Arrays.asList("1.first step", "2.second step", "3.third step"), Priority.HIGH, Severity.MAJOR);
        repository.createBug(VALID_TEAM_NAME, VALID_BOARD_NAME, SECOND_TITLE, VALID_DESCRIPTION,
                Arrays.asList("1.first step", "2.second step", "3.third step"), Priority.MEDIUM, Severity.CRITICAL);
        repository.createFeedback(VALID_TEAM_NAME, VALID_BOARD_NAME, THIRD_TITLE, VALID_DESCRIPTION);
        repository.createBug(VALID_TEAM_NAME, VALID_BOARD_NAME, FOURTH_TITLE, VALID_DESCRIPTION,
                Arrays.asList("1.first step", "2.second step", "3.third step"), Priority.HIGH, Severity.MAJOR);

        command = new ListAllTasks(repository);
    }

    @Test
    public void should_ThrowException_When_InvalidArgumentCount() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(SORT,"error")));
    }

    @Test
    public void execute_Should_ThrowException_when_FilterCommandWrong() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(WRONG_FILTER,FIRST_TITLE)));
    }

    @Test
    public void execute_Should_ThrowException_when_SortingCommandWrong() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(WRONG_SORT)));
    }

    @Test
    public void execute_Should_ThrowException_when_FilterParameterWrong() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(FILTER, "wrong","wrong")));
    }

    @Test
    public void execute_Should_ThrowException_when_FilterParameterMissing() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(FILTER)));
    }

    @Test
    public void execute_Should_ThrowException_when_FilterParameterUnsupported() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(FILTER,"1")));
    }

    @Test
    public void execute_Should_PrintAll_when_NoFilterNoSort() {
        String tasks = command.execute(Arrays.asList());
        StringBuilder sb = new StringBuilder();
        sb.append(LIST_SEPARATORS).append(System.lineSeparator())
                .append(ALL_TASKS).append(System.lineSeparator());
        String result = repository.findTaskById(1).print()+System.lineSeparator()+
                repository.findTaskById(2).print() +System.lineSeparator()+
                repository.findTaskById(3).print() +System.lineSeparator()+
                repository.findTaskById(4).print();
        sb.append(result).append(System.lineSeparator()).append(LIST_SEPARATORS);
        Assertions.assertEquals(sb.toString(), tasks);
    }

    @Test
    public void execute_Should_Filter_when_FilterCorrect() {

        String task = command.execute(Arrays.asList(FILTER, FIRST_TITLE));
        String target =String.format("%s%n%s%nID: 1, Title: %s%nDescription: %s%nStatus: ACTIVE%n%s%n%s",LIST_SEPARATORS,ALL_TASKS,FIRST_TITLE,VALID_DESCRIPTION,SPACER_MODELS,LIST_SEPARATORS);
        Assertions.assertEquals(target, task);
    }

    @Test
    public void execute_Should_Sort_when_SortCorrect() {
        String task = command.execute(Arrays.asList(SORT));
        StringBuilder sb = new StringBuilder();
        sb.append(LIST_SEPARATORS).append(System.lineSeparator())
                .append(ALL_TASKS).append(System.lineSeparator());
        sb.append(repository.findTaskById(4).print()).append(System.lineSeparator())
                .append(repository.findTaskById(3).print()).append(System.lineSeparator())
                .append(repository.findTaskById(2).print()).append(System.lineSeparator())
                .append(repository.findTaskById(1).print());
        sb.append(System.lineSeparator())
                .append(LIST_SEPARATORS);
        Assertions.assertEquals(sb.toString(), task);
    }
}
