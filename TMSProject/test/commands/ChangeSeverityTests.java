package commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.changecommands.ChangeSeverity;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.task.Bug;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Severity;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class ChangeSeverityTests {

    public static final String CRITICAL_SEVERITY = "Critical";
    public static final String MAJOR_SEVERITY = "Major";
    public static final String VALID_ID = "1";
    public static final String INVALID_ID = "3";
    public static final String INVALID_SEVERITY = "This is an invalid Severity";
    TSMRepository tsmRepository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        tsmRepository = new TSMRepositoryImpl();
        command = new ChangeSeverity(tsmRepository);

        tsmRepository.createTeam(VALID_TEAM_NAME);
        tsmRepository.createBoardInTeam(VALID_TEAM_NAME, VALID_BOARD_NAME);
        tsmRepository.createPerson(VALID_PERSON);

        createValidCriticalSeverityBugInRepo();
    }

    private void createValidCriticalSeverityBugInRepo() {
        List<String> helper = List.of(VALID_STEPS_TO_REPRODUCE.split("[1-9]+[.]"));
        List<String> validStepsToReproduce = helper
                .stream()
                .map(String::trim)
                .collect(Collectors.toList());

        Priority priority = Priority.valueOf(VALID_PRIORITY);

        //initial severity is CRITICAL
        Severity severity = Severity.valueOf(CRITICAL_SEVERITY.toUpperCase());

        tsmRepository.createBug(VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_TASK_TITLE, VALID_DESCRIPTION,
                validStepsToReproduce, priority, severity);
    }

    @Test
    public void execute_Should_ChangeSeverity_When_ValidParameters() {
        command.execute(Arrays.asList(VALID_ID, MAJOR_SEVERITY, VALID_PERSON));
        Bug bug = (Bug) tsmRepository.findTaskById(1);

        assertEquals(Severity.MAJOR, bug.getSeverity());
    }

    @Test
    public void execute_Should_ChangeSeverity_When_ValidParametersWithWhiteSpace() {
        Bug bug = (Bug) tsmRepository.findTaskById(1);

        assertDoesNotThrow(() -> command.execute(
                Arrays.asList("  " + VALID_ID + "  ", "  " + MAJOR_SEVERITY + "  ", "  " + VALID_PERSON)));
        assertEquals(Severity.MAJOR, bug.getSeverity());
    }

    @Test
    public void execute_Should_ThrowException_when_InvalidParameterCount() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(VALID_ID)));
    }

    @Test
    public void execute_Should_ThrowException_when_InvalidSeverity() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(VALID_ID, INVALID_SEVERITY, VALID_PERSON)));
    }

    @Test
    public void execute_Should_ThrowException_when_InvalidTaskId() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(INVALID_ID, MAJOR_SEVERITY, VALID_PERSON)));
    }

    @Test
    public void execute_Should_ThrowException_when_InvalidPerson() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(INVALID_ID, MAJOR_SEVERITY, INVALID_PERSON_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_when_SeverityIsTheSame() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(VALID_ID, CRITICAL_SEVERITY, VALID_PERSON)));
    }
}
