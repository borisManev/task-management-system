package commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.AddPerson;
import taskmanagementsystem.commands.showcommands.ShowAllTeamMembers;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.PersonImpl;
import taskmanagementsystem.models.TeamImpl;
import taskmanagementsystem.models.contracts.Person;
import taskmanagementsystem.models.contracts.Team;

import java.util.List;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ShowAllTeamMembersTests {
    // boris

    private ShowAllTeamMembers showAllTeamMembers;
    private TSMRepository repository;

    @BeforeEach
    public void before(){
        repository = new TSMRepositoryImpl();
        showAllTeamMembers = new ShowAllTeamMembers(repository);
    }

    @Test
    public void should_ThrowException_When_NotValidArgumentsCount() {
        // Arrange, Act, Assert
        assertThrows(InvalidUserInputException.class, () -> showAllTeamMembers.execute(List.of()));
    }

    @Test
    public void execute_ShouldNotThrow_When_InputValid(){
        AddPerson addPerson = new AddPerson(repository);
        Team team = new TeamImpl(VALID_TEAM_NAME);
        repository.createTeam(team.getName());
        Person person = new PersonImpl(VALID_PERSON);
        repository.createPerson(person.getName());
        List<String> arguments1 = List.of(team.getName(),person.getName());
        addPerson.execute(arguments1);

        team.addMember(person);
        List<String> arguments = List.of(team.getName() );

        Assertions.assertDoesNotThrow(()->showAllTeamMembers.execute(arguments));
        Assertions.assertEquals(1, repository.getTeams().get(0).getMembers().size());

    }

//    @Test
//    public void execute_ShouldPrint_When_InputValid(){
//        AddPerson addPerson = new AddPerson(repository);
//        Team team = new TeamImpl(VALID_TEAM_NAME);
//        repository.createTeam(team.getName());
//        Person person = new PersonImpl(VALID_PERSON);
//        repository.createPerson(person.getName());
//        List<String> arguments1 = List.of(team.getName(),person.getName());
//        addPerson.execute(arguments1);
//
//        team.addMember(person);
//        List<String> arguments = List.of(team.getName() );
//
//        Assertions.assertDoesNotThrow(()->showAllTeamMembers.execute(arguments));
//        Assertions.assertEquals(1, repository.getTeams().get(0).getMembers().size());
//
//    }
}
