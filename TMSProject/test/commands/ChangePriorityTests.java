package commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.changecommands.ChangePriority;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.task.Bug;

import java.util.Arrays;

import static Utils.TestUtils.*;

public class ChangePriorityTests {

    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        command = new ChangePriority(repository);
        createTestPerson(repository);
        createTestTeam(repository);
        repository.findTeamByName(VALID_TEAM_NAME).addMember(repository.findPersonByName(VALID_PERSON));
        repository.createBoardInTeam(VALID_TEAM_NAME, VALID_BOARD_NAME);
    }

    @Test
    public void execute_Should_ThrowException_when_NotValidParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(VALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_when_NotValidPriority() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("1", VALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_when_NotValidParam() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(INVALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ChangePriority_when_ValidParameters() {
        createTestBug(repository);
        command.execute(Arrays.asList("1", VALID_PRIORITY, VALID_PERSON));
        Bug bug = (Bug) repository.findTaskById(1);
        Assertions.assertEquals(VALID_PRIORITY, bug.getPriority().toString());
    }

    @Test
    public void execute_Should_ThrowException_when_NoSuchTask() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("1", VALID_PRIORITY)));
    }

    @Test
    public void execute_Should_ThrowException_when_PriorityAlreadyChanged() {
        createTestBug(repository);
        command.execute(Arrays.asList("1", VALID_PRIORITY, VALID_PERSON));
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("1", VALID_PRIORITY)));
    }
//Stanimir
}
