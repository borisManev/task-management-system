package commands;

import org.junit.jupiter.api.*;
import taskmanagementsystem.commands.AddComment;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.task.Comment;

import java.util.ArrayList;
import java.util.List;

import static Utils.TestUtils.*;

public class AddCommentTests {
    public static final String VALID_MESSAGE = "moneys are stolen";
    public static final String VALID_AUTHOR = "BRAND";


    public static final int VALID_ARGUMENTS = 2;
    public static final int VALID_ID = 1;
    public static final String INVALID_ID = "b";
    TSMRepository repository;
    Command addComment;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        addComment = new AddComment(repository);
    }

    @Test
    public void execute_Should_AddComment_When_ValidParameters() {
        createTestTeam(repository);
        createTestBoard(repository);
        repository.createPerson(VALID_PERSON);
        createTestBug(repository);
        List<String> parameters = new ArrayList<>();
        parameters.add(String.valueOf(VALID_ID));
        parameters.add(VALID_MESSAGE);
        parameters.add(VALID_PERSON);
        addComment.execute(parameters);

        Comment comment = repository.findTaskById(1).getComments().get(0);

        Assertions.assertEquals(VALID_MESSAGE, comment.getMessage());
    }
    @Test
    public void execute_Should_ThrowException_When_InvalidParameters() {
        List<String> parameters = new ArrayList<>(VALID_ARGUMENTS-1);

        Assertions.assertThrows(InvalidUserInputException.class, ()->addComment.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_InvalidNumber() {
        List<String> parameters = new ArrayList<>();
        parameters.add(INVALID_ID);
        parameters.add(VALID_MESSAGE);
        parameters.add(VALID_AUTHOR);

        Assertions.assertThrows(InvalidUserInputException.class, ()->addComment.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ID_DoesNotExist() {
        List<String> parameters = new ArrayList<>();
        parameters.add(String.valueOf(VALID_ID));
        parameters.add(VALID_MESSAGE);
        parameters.add(VALID_AUTHOR);


        Assertions.assertThrows(InvalidUserInputException.class, ()->addComment.execute(parameters));
    }
//Boris
}
