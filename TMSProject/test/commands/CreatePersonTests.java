package commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.createcommands.CreatePerson;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.DuplicateEntityException;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.List;

import static Utils.TestUtils.*;
import static Utils.TestUtils.VALID_BOARD_NAME;

public class CreatePersonTests {

    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        command = new CreatePerson(repository);
        createTestPerson(repository);
        createTestTeam(repository);
        repository.findTeamByName(VALID_TEAM_NAME).addMember(repository.findPersonByName(VALID_PERSON));
        repository.createBoardInTeam(VALID_TEAM_NAME, VALID_BOARD_NAME);
    }

    @Test
    public void execute_Should_ThrowException_When_MoreParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(VALID_PERSON, INVALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_When_NoParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(List.of()));
    }

    @Test
    public void execute_Should_ThrowException_When_PersonExists() {
        Assertions.assertThrows(DuplicateEntityException.class, () -> command.execute(List.of(VALID_PERSON)));
    }

    @Test
    public void execute_Should_CreatePerson_When_ParametersCorrect() {
        Assertions.assertEquals(1,repository.getPeople().size());
    }

}
