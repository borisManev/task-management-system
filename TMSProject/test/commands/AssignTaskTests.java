package commands;

import org.junit.jupiter.api.*;
import taskmanagementsystem.commands.AssignTask;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;


import java.util.Arrays;

import static Utils.TestUtils.*;

public class AssignTaskTests {

    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        command = new AssignTask(repository);
        createTestPerson(repository);
        createTestTeam(repository);
        repository.findTeamByName(VALID_TEAM_NAME).addMember(repository.findPersonByName(VALID_PERSON));
        repository.createBoardInTeam(VALID_TEAM_NAME, VALID_BOARD_NAME);
        createTestBug(repository);
     }

    @Test
    public void execute_Should_ThrowException_when_LessParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(VALID_PERSON)));
    }

    @Test
    public void execute_Should_ThrowException_when_MoreParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(VALID_PERSON, "15", "1", "1")));
    }

    @Test
    public void execute_Should_ThrowException_when_ParametersWrong() {
        Assertions.assertThrows(NumberFormatException.class, () -> command.execute(Arrays.asList(VALID_PERSON, "15")));
    }

    @Test
    public void execute_Should_ThrowException_when_TaskAlreadyAssigned() {
        command.execute(Arrays.asList(VALID_PERSON, "1"));
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(VALID_PERSON, "1" )));
    }

    @Test
    public void execute_Should_AssaignTask_when_ParametersCorrect() {
        command.execute(Arrays.asList(VALID_PERSON, "1"));
        Assertions.assertTrue(repository.findTaskById(1).hasAssignee());
    }
    @Test
    public void execute_Should_ThrowException_when_TaskAndUserInDifferentTeam() {
        repository.createTeam("newTeam");
        repository.createPerson("newPerson");
        repository.findTeamByName("newTeam").addMember(repository.findPersonByName("newPerson"));

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("newPerson", "1")));
    }
//Stanimir
}
