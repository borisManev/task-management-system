package commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.createcommands.CreateBoard;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.List;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CreateBoardTests {

    TSMRepository tsmRepository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        tsmRepository = new TSMRepositoryImpl();
        command = new CreateBoard(tsmRepository);

        tsmRepository.createTeam(VALID_TEAM_NAME);
    }

    @Test
    public void execute_Should_CreateBoard_When_ValidParameters() {
        command.execute(List.of(VALID_TEAM_NAME, VALID_BOARD_NAME));

        assertEquals(1, tsmRepository.getBoards().size());
    }

    @Test
    public void execute_Should_CreateBoard_When_ValidParametersWithWhiteSpace() {
        command.execute(List.of("  " + VALID_TEAM_NAME, VALID_BOARD_NAME + "  "));

        assertEquals(1, tsmRepository.getBoards().size());
    }

    @Test
    public void execute_Should_ThrowException_When_InvalidArgumentCount() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(VALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_When_TeamNameIsInvalid() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(INVALID_TEAM_NAME, VALID_BOARD_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_When_BoardNameIsInvalid() {
        assertThrows(IllegalArgumentException.class, () -> command.execute(List.of(VALID_TEAM_NAME, INVALID_BOARD_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_When_BoardNameAlreadyExistsInTeam() {
        tsmRepository.createBoardInTeam(VALID_TEAM_NAME, VALID_BOARD_NAME);

        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(VALID_TEAM_NAME, VALID_BOARD_NAME)));
    }
}
