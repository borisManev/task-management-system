package commands;

import org.junit.jupiter.api.*;
import taskmanagementsystem.commands.showcommands.ShowAllTeamBoards;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.Arrays;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static taskmanagementsystem.models.TaskBase.SPACER_MODELS;

public class ShowAllTeamBoardsTests {

    public static final String TEST_TEAM = "TestTeam";
    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        createTestTeam(repository);
        createTestBoard(repository);
        command = new ShowAllTeamBoards(repository);
    }
    @Test
    public void execute_Should_ShowAllTeamBoards_when_ValidParameters() {
        String target =SPACER_MODELS+System.lineSeparator()+"Board: yyyyy"+System.lineSeparator() + "#No tasks#"+System.lineSeparator()+SPACER_MODELS;
        String result = command.execute(Arrays.asList(VALID_TEAM_NAME));
           assertTrue(target.equals(result));
    }
    @Test
    public void execute_Should_Throw_when_ParametersNotValid() {
       assertThrows(InvalidUserInputException.class,() -> command.execute(Arrays.asList("NotValid")));
    }

    @Test
    public void execute_Should_Throw_when_ParameterEmpty() {
        assertThrows(InvalidUserInputException.class,() -> command.execute(Arrays.asList("")));
    }
    @Test
    public void execute_Should_ShowAllTeamBoards_when_NoBoards() {
        String target ="#No boards#";
        repository.createTeam(TEST_TEAM);
        String result = command.execute(Arrays.asList(TEST_TEAM));
        assertTrue(target.equals(result));
    }

    //Stanimir
}
