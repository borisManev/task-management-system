package commands;
import org.junit.jupiter.api.*;
import taskmanagementsystem.commands.AddPerson;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.Arrays;

import static Utils.TestUtils.*;

public class AddPersonTests {

    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        createTestPerson(repository);
        createTestTeam(repository);
        command = new AddPerson(repository);
    }

    @Test
    public void execute_Should_AddPersonToTeam_when_ValidParameters() {
        command.execute(Arrays.asList(VALID_TEAM_NAME, VALID_PERSON));

        Assertions.assertEquals(1, repository.findTeamByName(VALID_TEAM_NAME).getMembers().size());
    }


    @Test
    public void execute_Should_ThrowException_when_MoreParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(VALID_TEAM_NAME, VALID_PERSON, INVALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_when_LessParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(VALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_when_PersonNotExists() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(VALID_TEAM_NAME, INVALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_when_TeamNotExists() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(INVALID_TEAM_NAME, VALID_PERSON)));
    }

    @Test
    public void execute_Should_ThrowException_when_PersonAlreadyAdded() {
        command.execute(Arrays.asList(VALID_TEAM_NAME, VALID_PERSON));
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(VALID_TEAM_NAME, VALID_PERSON)));
    }
//Stanimir
}
