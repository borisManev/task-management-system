package commands;

import org.junit.jupiter.api.*;
import taskmanagementsystem.commands.ListAllTasksWithActiveAssignee;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Size;

import java.util.Arrays;

import static Utils.TestUtils.*;


public class ListAllTasksWithActiveAssigneeTests {
    TSMRepository repository;
    Command command;
    public  static final String No_FILTER = "no filter";
    public  static final String FILTER = "FILTER_BY_ASSIGNEE";
    public  static final String FILTER_STATUS = "FILTER BY STATUS";
    public  static final String FILTER_ASSIGNEE_STATUS = "FILTER_BY_ASSIGNEE_AND_STATUS";
    public  static final String Wrong_FILTER = "Wrong_FILTER";
    public  static final String No_SORT ="no sort";
    public  static final String SORT ="SORT BY TITLE";



    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        createTestPerson(repository);
        createTestTeam(repository);
        createTestBoard(repository);
        createTestBug(repository);
        repository.getAssignableTasks().get(0).assignPerson(VALID_PERSON);
        command = new ListAllTasksWithActiveAssignee(repository);
    }

    @Test
    public void execute_Should_ThrowException_when_LessParameters() {
        Assertions.assertThrows(InvalidUserInputException.class,()-> command.execute(Arrays.asList(No_FILTER)));
    }

    @Test
    public void execute_Should_ThrowException_when_FilterCommandWrong() {
        Assertions.assertThrows(InvalidUserInputException.class,()-> command.execute(Arrays.asList(Wrong_FILTER,No_SORT)));
    }
   @Test
   public void execute_Should_ThrowException_when_FilterParameterWrong() {
       Assertions.assertThrows(InvalidUserInputException.class,()-> command.execute(Arrays.asList(FILTER_STATUS,No_SORT,"wrong")));
   }
    @Test
    public void execute_Should_ThrowException_when_FilterParameterMissing() {
        Assertions.assertThrows(InvalidUserInputException.class,()-> command.execute(Arrays.asList(FILTER,No_SORT)));
    }

    @Test
    public void execute_Should_Filter_when_FilterParameterCorrect() {
        String tasks = command.execute(Arrays.asList(No_FILTER,No_SORT));
        String target =String.format("==========%nList tasks with active Assignee:%n%s==========",repository.findTaskById(1).toString());
        Assertions.assertEquals(target, tasks);
    }

    @Test
    public void execute_Should_Filter_when_FilterCorrect() {
        String task = command.execute(Arrays.asList(FILTER,No_SORT,VALID_PERSON));
        String target =String.format("==========%nList tasks with active Assignee:%n%s==========",repository.getAssignableTasks().get(0).toString());
        Assertions.assertEquals(target, task);
    }


    @Test
    public void execute_Should_FilterAndSort_when_ParameterCorrect() {
        repository.createStory(VALID_TEAM_NAME, VALID_BOARD_NAME, "secondTitle", VALID_DESCRIPTION,  Priority.HIGH, Size.LARGE);
        repository.getAssignableTasks().get(1).assignPerson(VALID_PERSON);
        String tasks = command.execute(Arrays.asList(No_FILTER,SORT));
        String target =String.format("==========%nList tasks with active Assignee:%n%s%s==========",repository.getAssignableTasks().get(0).toString(),repository.getTasks().get(1).toString());
        Assertions.assertEquals(target, tasks);
    }

    @Test
    public void execute_Should_FilterFilterAndSort_when_ParameterCorrect() {
        repository.createStory(VALID_TEAM_NAME, VALID_BOARD_NAME, "secondTitle", VALID_DESCRIPTION,  Priority.HIGH, Size.LARGE);
        repository.getAssignableTasks().get(1).assignPerson(VALID_PERSON);
        String tasks = command.execute(Arrays.asList(FILTER_ASSIGNEE_STATUS,SORT,VALID_PERSON,"NOTDONE"));
        String target =String.format("==========%nList tasks with active Assignee:%n%s==========",repository.getTasks().get(1).toString());
        Assertions.assertEquals(target, tasks);
    }



}
