package commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.createcommands.CreateTeam;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.List;

import static Utils.TestUtils.INVALID_TEAM_NAME;
import static Utils.TestUtils.VALID_TEAM_NAME;
import static org.junit.jupiter.api.Assertions.*;

public class CreateTeamTests {

    TSMRepository tsmRepository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        tsmRepository = new TSMRepositoryImpl();
        command = new CreateTeam(tsmRepository);
    }

    @Test
    public void execute_Should_CreateTeam_When_ArgumentsAreValid() {
        command.execute(List.of(VALID_TEAM_NAME));

        assertEquals(1, tsmRepository.getTeams().size());
    }

    @Test
    public void execute_Should_CreateTeam_When_ArgumentsAreValidWithWhiteSpace() {

        assertDoesNotThrow(() -> command.execute(List.of("  " + VALID_TEAM_NAME + "  ")));
        assertEquals(1, tsmRepository.getTeams().size());
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentCountIsInvalid() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of()));
    }

    @Test
    public void execute_Should_ThrowException_When_TeamNameIsInvalid() {
        assertThrows(IllegalArgumentException.class, () -> command.execute(List.of(INVALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_When_TeamAlreadyExists() {
        tsmRepository.createTeam(VALID_TEAM_NAME);
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(VALID_TEAM_NAME)));
    }
}
