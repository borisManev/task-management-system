package commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.showcommands.ShowAllTeams;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.List;

import static Utils.TestUtils.VALID_TEAM_NAME;
import static org.junit.jupiter.api.Assertions.*;

public class ShowAllTeamsTests {

    private Command command;
    private TSMRepository repository;

    @BeforeEach
    public void before() {
        repository = new TSMRepositoryImpl();
        command = new ShowAllTeams(repository);
    }

    @Test
    public void should_ThrowException_When_InvalidArgumentCount() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of("argument")));
    }

    @Test
    public void execute_ShouldReturnAString_When_InputValid() {
        repository.createTeam(VALID_TEAM_NAME);

        assertDoesNotThrow(() -> command.execute(List.of()));
        assertFalse(command.execute(List.of()).isBlank());
    }
}
