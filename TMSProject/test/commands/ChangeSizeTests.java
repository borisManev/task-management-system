package commands;

import org.junit.jupiter.api.*;
import taskmanagementsystem.commands.changecommands.ChangeSize;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.task.Story;

import java.util.Arrays;

import static Utils.TestUtils.*;

public class ChangeSizeTests {

    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        createTestPerson(repository);
        createTestTeam(repository);
        repository.createBoardInTeam(VALID_TEAM_NAME, VALID_BOARD_NAME);
        command = new ChangeSize(repository);
        createTestStory(repository);
        createTestBug(repository);
    }

    @Test
    public void execute_Should_ThrowException_when_ParametersNotValid() {
      Assertions.assertThrows(InvalidUserInputException.class, ()->command.execute(Arrays.asList(INVALID_TEAM_NAME)));
    }
    @Test
    public void execute_Should_ThrowException_when_TaskNotStory() {
        Assertions.assertThrows(InvalidUserInputException.class, ()->command.execute(Arrays.asList("2",VALID_SIZE)));
    }
    @Test
    public void execute_Should_ThrowException_when_NotCorrectSize() {
        Assertions.assertThrows(InvalidUserInputException.class, ()->command.execute(Arrays.asList("1", INVALID_TEAM_NAME)));
    }
    @Test
    public void execute_Should_ChangeSize_when_ParametersValid() {
        command.execute(Arrays.asList("1",VALID_SIZE,VALID_PERSON));
        Story story =(Story) repository.findTaskById(1);
        Assertions.assertEquals(VALID_SIZE,story.getSize().toString());
    }
    @Test
    public void execute_Should_ThrowException_when_SizeAlreadyChanged() {
        command.execute(Arrays.asList("1",VALID_SIZE,VALID_PERSON));
        Assertions.assertThrows( InvalidUserInputException.class, ()-> command.execute(Arrays.asList("1",VALID_SIZE)));
    }
    //Stanimir
}
