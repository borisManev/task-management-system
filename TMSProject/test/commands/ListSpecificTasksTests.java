package commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.ListAllTasksWithActiveAssignee;
import taskmanagementsystem.commands.ListSpecificTasks;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.Arrays;

import static Utils.TestUtils.*;

public class ListSpecificTasksTests {
    // boris

    TSMRepository repository;
    Command command;

    public  static final String LONG_FILTER = "FILTER FILTER FILTER FILTER FILTER FILTER FILTER FILTER FILTER FILTER";

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        createTestPerson(repository);
        createTestTeam(repository);
        createTestBoard(repository);
        createTestBug(repository);
        repository.getAssignableTasks().get(0).assignPerson(VALID_PERSON);
        command = new ListSpecificTasks(repository);
    }

    @Test
    public void execute_Should_ThrowException_when_LessParameters() {
        Assertions.assertThrows(InvalidUserInputException.class,()-> command.execute(Arrays.asList()));
    }


    @Test
    public void execute_Should_Filter_when_FilterCorrectTaskName() {
        String task = command.execute(Arrays.asList(VALID_TASK_TITLE));
        Assertions.assertEquals(repository.getTasks().get(0).getTitle().toString(), VALID_TASK_TITLE);

    }

}
