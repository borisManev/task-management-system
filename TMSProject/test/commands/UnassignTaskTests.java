package commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.UnassignTask;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.Arrays;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UnassignTaskTests {

    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        createTestTeam(repository);
        createTestBoard(repository);
        createTestPerson(repository);
        createTestBug(repository);
        repository.getAssignableTasks().get(0).assignPerson(VALID_PERSON);
        command = new UnassignTask(repository);
    }

    @Test
    public void execute_Should_Throw_when_ParametersMore() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("1", VALID_PERSON, VALID_PERSON)));
    }

    @Test
    public void execute_Should_Throw_when_ParametersBlank() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("", VALID_PERSON)));
    }

    @Test
    public void execute_Should_Throw_when_TaskMissing() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("2", VALID_PERSON)));
    }

    @Test
    public void execute_Should_Throw_when_TaskNotAssigned() {
        command.execute(Arrays.asList("1", VALID_PERSON));
        assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("1")));
    }

    @Test
    public void execute_Should_UnassignedTask_when_AllCorrect() {
        command.execute(Arrays.asList("1", VALID_PERSON));
        assertTrue(!repository.findTaskById(1).hasAssignee());
    }

    //Stanimir
}
