package commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.changecommands.ChangeRating;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;
import taskmanagementsystem.models.contracts.task.Feedback;

import java.util.Arrays;
import java.util.List;

import static Utils.TestUtils.*;
import static Utils.TestUtils.VALID_BOARD_NAME;

public class ChangeRatingTests {

    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        command = new ChangeRating(repository);
        createTestPerson(repository);
        createTestTeam(repository);
        repository.findTeamByName(VALID_TEAM_NAME).addMember(repository.findPersonByName(VALID_PERSON));
        repository.createBoardInTeam(VALID_TEAM_NAME, VALID_BOARD_NAME);
    }
    @Test
    public void execute_Should_ThrowException_When_NotValidParameters() {
        Assertions.assertThrows(InvalidUserInputException.class,()->command.execute(List.of(VALID_TEAM_NAME)));
    }
    @Test
    public void execute_Should_ThrowException_When_NotValidRating() {
        Assertions.assertThrows(NumberFormatException.class,()->command.execute(Arrays.asList("1", VALID_TEAM_NAME, VALID_PERSON)));
    }
    @Test
    public void execute_Should_ThrowException_When_NotValidParam() {
        Assertions.assertThrows(InvalidUserInputException.class,()->command.execute(List.of(INVALID_TEAM_NAME)));
    }
    @Test
    public void execute_Should_ThrowException_When_NoSuchTask() {
        Assertions.assertThrows(InvalidUserInputException.class, ()->command.execute(List.of("1", VALID_RATING)));
    }
    @Test
    public void execute_Should_ChangeRating_When_ValidParameters() {
        createTestFeedback(repository);
        command.execute(Arrays.asList("1", VALID_RATING, VALID_PERSON));
        Feedback feedback = (Feedback) repository.findTaskById(1);
        Assertions.assertEquals(VALID_RATING,String.valueOf(feedback.getRating()));
    }


    //boris

}
