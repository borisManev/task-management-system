package commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.showcommands.ShowTeamActivity;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.List;

import static Utils.TestUtils.VALID_TEAM_NAME;
import static org.junit.jupiter.api.Assertions.*;

public class ShowTeamActivityTests {

    private Command command;
    private TSMRepository repository;

    @BeforeEach
    public void before() {
        repository = new TSMRepositoryImpl();
        command = new ShowTeamActivity(repository);
    }

    @Test
    public void should_ThrowException_When_InvalidArgumentCount() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of()));
    }

    @Test
    public void execute_ShouldReturnAString_When_InputValid() {
        repository.createTeam(VALID_TEAM_NAME);

        assertDoesNotThrow(() -> command.execute(List.of(VALID_TEAM_NAME)));
        assertFalse(command.execute(List.of(VALID_TEAM_NAME)).isBlank());
    }
}
