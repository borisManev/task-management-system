package commands;

import org.junit.jupiter.api.*;
import taskmanagementsystem.commands.createcommands.CreateTask;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;


import java.util.Arrays;

import static Utils.TestUtils.*;


public class CreateTaskTests {

    TSMRepository repository;
    Command command;

    @BeforeEach
    public void beforeEach() {
        repository = new TSMRepositoryImpl();
        command = new CreateTask(repository);
        createTestPerson(repository);
        createTestTeam(repository);
        repository.findTeamByName(VALID_TEAM_NAME).addMember(repository.findPersonByName(VALID_PERSON));
        repository.createBoardInTeam(VALID_TEAM_NAME, VALID_BOARD_NAME);
    }

    @Test
    public void execute_Should_ThrowException_when_LessParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList(INVALID_TEAM_NAME)));
    }

    @Test
    public void execute_Should_ThrowException_when_ParametersNotMatchTask() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("BUG", VALID_TASK_TITLE, VALID_DESCRIPTION,  "HIGH", "LARGE")));
    }
    @Test
    public void execute_Should_CreateStory_when_ParametersCorrect() {
        command.execute(Arrays.asList("STORY", VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_TASK_TITLE, VALID_DESCRIPTION, "HIGH", "LARGE"));
        Assertions.assertEquals(1,repository.getTasks().size());
    }
    @Test
    public void execute_Should_ThrowException_when_ParameterBlank() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(Arrays.asList("STORY", VALID_TASK_TITLE, " ",  "HIGH", "LARGE")));
    }
    @Test
    public void execute_Should_ThrowException_when_ParameterNotCorrect() {

        Assertions.assertThrows(InvalidUserInputException.class,()->command.execute(Arrays.asList("STORY", VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_TASK_TITLE, VALID_DESCRIPTION, "H", "LARGE")));
    }
    @Test
    public void execute_Should_CreateBug_when_ParametersCorrect() {
        command.execute(Arrays.asList("BUG", VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_TASK_TITLE, VALID_DESCRIPTION, "1.start. 2.go to. 3.here.", "HIGH", "MAJOR"));
        Assertions.assertEquals(1,repository.getTasks().size());
    }
    @Test
    public void execute_Should_ThrowException_when_NoStepsToReproduce() {
       Assertions.assertThrows(InvalidUserInputException.class, ()-> command.execute(Arrays.asList("BUG", VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_TASK_TITLE, VALID_DESCRIPTION, " ", "HIGH", "MAJOR")));

    }
    @Test
    public void execute_Should_CreateFeedback_when_ParametersCorrect() {
        command.execute(Arrays.asList("FEEDBACK", VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_TASK_TITLE, VALID_DESCRIPTION));
        Assertions.assertEquals(1,repository.getTasks().size());
    }

}
