package commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import taskmanagementsystem.commands.showcommands.ShowBoardActivity;
import taskmanagementsystem.commands.contracts.Command;
import taskmanagementsystem.core.TSMRepositoryImpl;
import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.exceptions.InvalidUserInputException;

import java.util.List;

import static Utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ShowBoardActivityTests {
    public static final String ACTIVITY_PRINT_PATTERN = "==========%nShow activity of board: yyyyy from team: xxxxx%n----------%n%s%n----------%n==========";
    private Command command;
    private TSMRepository repository;

    @BeforeEach
    public void before() {
        repository = new TSMRepositoryImpl();
        command = new ShowBoardActivity(repository);
        createTestTeam(repository);
        createTestBoard(repository);
    }

    @Test
    public void should_ThrowException_When_NoArguments() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of()));
    }

    @Test
    public void should_ThrowException_When_NotValidArgumentsCount() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(VALID_TEAM_NAME)));
    }

    @Test
    public void should_ThrowException_When_TeamNotValid() {
        assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(INVALID_TEAM_NAME,VALID_BOARD_NAME)));
    }
    @Test
    public void should_ShowActivityEmpty_When_ParametersValid() {
        String result = command.execute(List.of(VALID_TEAM_NAME,VALID_BOARD_NAME));
        String target=String.format(ACTIVITY_PRINT_PATTERN,"#No Activity#");
        assertEquals(target,result);
    }
    @Test
    public void should_ShowActivity_When_ParametersValid() {
        createTestBug(repository);
        String result = command.execute(List.of(VALID_TEAM_NAME,VALID_BOARD_NAME));
        String target=String.format(ACTIVITY_PRINT_PATTERN, repository.findTaskById(1).getHistory().getActivities().get(0).print().trim());
        assertEquals(target,result);
    }
}
