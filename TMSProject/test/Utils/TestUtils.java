package Utils;

import taskmanagementsystem.core.contracts.TSMRepository;
import taskmanagementsystem.models.TaskBase;
import taskmanagementsystem.models.enums.Priority;
import taskmanagementsystem.models.enums.Severity;
import taskmanagementsystem.models.enums.Size;

import java.util.Arrays;

import static taskmanagementsystem.models.BoardImpl.BOARD_NAME_MAX_LENGTH;
import static taskmanagementsystem.models.BoardImpl.BOARD_NAME_MIN_LENGTH;
import static taskmanagementsystem.models.PersonImpl.MAX_NAME_LENGTH;
import static taskmanagementsystem.models.PersonImpl.MIN_NAME_LENGTH;
import static taskmanagementsystem.models.TeamImpl.NAME_LEN_MIN;

public class TestUtils {
    public static final String VALID_TEAM_NAME = getStringX(NAME_LEN_MIN);
    public static final String INVALID_TEAM_NAME = getStringX(NAME_LEN_MIN-1);
    public static final String VALID_BOARD_NAME = getStringY(BOARD_NAME_MIN_LENGTH);
    public static final String INVALID_BOARD_NAME = getStringX(BOARD_NAME_MAX_LENGTH+1);
    public static final String VALID_PERSON = getStringZ(MIN_NAME_LENGTH);
    public static final String INVALID_PERSON_NAME = getStringX(MAX_NAME_LENGTH+1);
    public static final String VALID_TASK_TITLE = "BigBugBigBugBigBug";
    public static final String INVALID_TASK_TITLE = getStringX(TaskBase.TITLE_MAX_LENGTH+1);
    public static final String VALID_DESCRIPTION = "From time to time.";
    public static final String INVALID_DESCRIPTION = getStringX(TaskBase.DESCRIPTION_MAX_LENGTH+1);

    public static final String VALID_PRIORITY = "MEDIUM";
    public static final String VALID_RATING = "3";

    public static final String VALID_SIZE = "MEDIUM";
    public static final String VALID_COMMENT_CONTENT = "This comment content is valid.";
    public static final String VALID_STEPS_TO_REPRODUCE =
            "1.Go to home page. 2.Click home button. 3.Click options button.";
    public static final String VALID_SEVERITY = "CRITICAL";
    public static final String VALID_BUG_STATUS = "FIXED";
    public static final String INVALID_BUG_STATUS = "ACTIVESTATUS";


    public static String getStringX (int length){
        return "x".repeat(length);

    }
    public static String getStringY (int length){
        return "y".repeat(length);

    }
    public static String getStringZ (int length){
        return "z".repeat(length);

    }

    public static void createTestPerson(TSMRepository repository){
        repository.createPerson(VALID_PERSON);
    }


    public static void createTestTeam(TSMRepository repository){
       repository.createTeam(VALID_TEAM_NAME);
    }

    public static void createTestBoard(TSMRepository repository) {
        repository.createBoardInTeam(VALID_TEAM_NAME,VALID_BOARD_NAME);
    }
    public static void createTestBug(TSMRepository repository){
        repository.createBug(VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_TASK_TITLE, VALID_DESCRIPTION,
                Arrays.asList("1.start ", "2.go to ", "3.here."), Priority.HIGH, Severity.MAJOR);

    }
    public static void createTestStory(TSMRepository repository){
        repository.createStory(VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_TASK_TITLE, VALID_DESCRIPTION,  Priority.HIGH, Size.LARGE);

    }

    public static void createTestFeedback(TSMRepository repository){
        repository.createFeedback(VALID_TEAM_NAME, VALID_BOARD_NAME, VALID_TASK_TITLE, VALID_DESCRIPTION);

    }


}
