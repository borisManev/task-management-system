# Task Management System
* each roll is a command, till "Exit".
* command parameters split by " # ". 
* person can be member of only one team, person name need to be unique and containing alphabetic symbols only. 
* task can be created in existing team and board, and can be assigned to one member of the team only. 
  task can be modified only by assignee if assignable or registered persons if not assignable.  
* List commands have filters and sort options. Each list is separated with "==========" in its
  beginning and end, and different fields inside are separated with "----------".
* commands trow error messages with "!", for each wrong input,parameter or command.

input:
CreatePerson # #%^asgsd
CreatePerson # az
CreateTeam # Buda
CreatePerson # Atanas
CreatePerson # Borislav
CreatePerson # Dimitar
CreatePerson # Evgeni
CreateTeam # Buda123
CreateTeam # Buda123
CreateTeam # Exchange
AddPerson # Buda123 # Borislav
AddPerson # Buda123 # Borislav
CreateBoard # Buda123 # Autobahn
CreateBoard # Buda123 # AutoVia
CreateBoard # Buda12 # Autobahn
CreateBoard # Exchange # Bridges
CreateTask # BUG # Buda123 # Autobahn # BigBugBigBugBigBug # from time to time # 1.start program. 2.go to properties. 3.here it is. # HIGH # MAJOR
CreateTask # STORY # Buda123 # Autobahn # TestCreateStory # Great Story  # HIGH # LARGE
CreateTask # FEEDBACK # Buda123 # Autobahn # TestFeedback # It will be strange if works.
CreateTask # BUG # Exchange # Bridges # BigBugBigBugBigBug # from time to time # 1.start program. 2.go to properties. 3.here it is. # HIGH # MAJOR
AddPerson # Exchange # Evgeni
AssignTask # Dimitar # 4
AssignTask # Borislav # 4
AssignTask # Evgeni # 4
AssignTask # Borislav # 1
AddComment # 1 # nothing is working # Borislav
ChangeStatus # 1 # FIXED # Borislav
ChangeRating # 1 # 7 # Atanas
AddPerson # Buda123 # Atanas
UnassignTask # 1
UnassignTask # 1 # Wrongname
UnassignTask # 1 # Borislav
AssignTask # Atanas # 1
AssignTask # Atanas # 2
AssignTask # Borislav # 3
AddComment # 2 # not accepting comands # NotReged
ChangeStatus # 1 # DONE # Borislav
ChangeStatus # 1 # DONE # Atanas
ChangeStatus # 1 # FIXED # Atanas
ChangeRating # 2 # 9 # Borislav
ChangeStatus # 3 # SCHEDULED # Atanas
ListAllTasksWithActiveAssignee # No_filter # Sort_by_Title
UnassignTask # 2
ListAllTasksWithActiveAssignee # No_filter # Sort_by_Title
CreateTeam # Deputat
ShowAllTeams
ShowAllTeamMembers # Buda123
ShowAllTeamMembers # NoTeam
ChangeStatus # 4 # FIXED # Evgeni
AddComment # 4 # bug solved # Evgeni
ListAllTasksWithActiveAssignee # no Filter # no Sort
ShowTeamActivity # Buda123
ShowPersonActivity # Atanas
ShowBoardActivity # AutoVia
ShowBoardActivity # Buda123 # Autobahn
ListAllTasks # Sort
Exit











output:
Person name contains invalid symbols!
Person name should be between 5 and 10 symbols!
Team name should be between 5 and 15 symbols!
Created new person with name: Atanas.
Created new person with name: Borislav.
Created new person with name: Dimitar.
Created new person with name: Evgeni.
Team Buda123 - created successfully.
Team with this name already exists!
Team Exchange - created successfully.
Borislav added - to team: Buda123 successfully.
Borislav is already in Buda123!
Board: Autobahn - created successfully in team: Buda123.
Board: AutoVia - created successfully in team: Buda123.
Team named Buda12 does not exist!
Board: Bridges - created successfully in team: Exchange.
Task type:Bug - created successfully.
Task type:Story - created successfully.
Task type:Feedback - created successfully.
Task type:Bug - created successfully.
Evgeni added - to team: Exchange successfully.
No member with name: Dimitar!
Member: Borislav is in team: Buda123, Task with id: 4 is in team: Exchange, command not accepted!
Task: 4 assigned to Evgeni.
Task: 1 assigned to Borislav.
Comment added to task: 1 by: Borislav.
Status of task: 1, changed to FIXED.
Task with id: 1, is not a Feedback and cannot be rated!
Atanas added - to team: Buda123 successfully.
Expected: 2, Received: 1. Invalid number of arguments!
Member does not exist!
Unassigned person from task with id:1 successfully.
Task: 1 assigned to Atanas.
Task: 2 assigned to Atanas.
Task is not assignable!
Author is not registered, need to be create person first!
Task with ID: 1 is assigned to Atanas and cannot by modified by Borislav!
DONE is not a valid status for bug!
Status of task: 1, changed to FIXED.
Task with id: 2, is not a Feedback and cannot be rated!
Status of task: 3, changed to SCHEDULED.
==========
List tasks with active Assignee:
----------
ID: 1, Title: BigBugBigBugBigBug
Description: from time to time
Priority: HIGH, Assignee: Atanas
Severity: MAJOR, Status: FIXED
Steps to reproduce:
1. start program.
2. go to properties.
3. here it is.
----------
----------
Comments:
Content: nothing is working, Author: Borislav
----------
----------
History:
Activity: Bug with id:1 created., time: 2022-12-30 20:34:35
Activity: Task with id: 1 - assigned to Borislav., time: 2022-12-30 20:34:35
Activity: Comment added to task: 1 by: Borislav., time: 2022-12-30 20:34:35
Activity: Assignee Borislav was unassigned by Borislav., time: 2022-12-30 20:34:35
Activity: Task with id: 1 - assigned to Atanas., time: 2022-12-30 20:34:35
----------
----------
ID: 4, Title: BigBugBigBugBigBug
Description: from time to time
Priority: HIGH, Assignee: Evgeni
Severity: MAJOR, Status: ACTIVE
Steps to reproduce:
1. start program.
2. go to properties.
3. here it is.
----------
----------
#No Comments#
----------
----------
History:
Activity: Bug with id:4 created., time: 2022-12-30 20:34:35
Activity: Task with id: 4 - assigned to Evgeni., time: 2022-12-30 20:34:35
----------
----------
ID: 2, Title: TestCreateStory
Description: Great Story
Priority: HIGH, Assignee: Atanas
----------
----------
#No Comments#
----------
----------
History:
Activity: Story with id:2 created., time: 2022-12-30 20:34:35
Activity: Task with id: 2 - assigned to Atanas., time: 2022-12-30 20:34:35
----------
==========
Expected: 2, Received: 1. Invalid number of arguments!
==========
List tasks with active Assignee:
----------
ID: 1, Title: BigBugBigBugBigBug
Description: from time to time
Priority: HIGH, Assignee: Atanas
Severity: MAJOR, Status: FIXED
Steps to reproduce:
1. start program.
2. go to properties.
3. here it is.
----------
----------
Comments:
Content: nothing is working, Author: Borislav
----------
----------
History:
Activity: Bug with id:1 created., time: 2022-12-30 20:34:35
Activity: Task with id: 1 - assigned to Borislav., time: 2022-12-30 20:34:35
Activity: Comment added to task: 1 by: Borislav., time: 2022-12-30 20:34:35
Activity: Assignee Borislav was unassigned by Borislav., time: 2022-12-30 20:34:35
Activity: Task with id: 1 - assigned to Atanas., time: 2022-12-30 20:34:35
----------
----------
ID: 4, Title: BigBugBigBugBigBug
Description: from time to time
Priority: HIGH, Assignee: Evgeni
Severity: MAJOR, Status: ACTIVE
Steps to reproduce:
1. start program.
2. go to properties.
3. here it is.
----------
----------
#No Comments#
----------
----------
History:
Activity: Bug with id:4 created., time: 2022-12-30 20:34:35
Activity: Task with id: 4 - assigned to Evgeni., time: 2022-12-30 20:34:35
----------
----------
ID: 2, Title: TestCreateStory
Description: Great Story
Priority: HIGH, Assignee: Atanas
----------
----------
#No Comments#
----------
----------
History:
Activity: Story with id:2 created., time: 2022-12-30 20:34:35
Activity: Task with id: 2 - assigned to Atanas., time: 2022-12-30 20:34:35
----------
==========
Team Deputat - created successfully.
==========
List all Teams :
----------
Team name: Buda123
Team name: Exchange
Team name: Deputat
----------
==========
==========
List all members of team: Buda123
----------
member: Borislav
member: Atanas
----------
==========
There is no team with name: NoTeam
Status of task: 4, changed to FIXED.
Comment added to task: 4 by: Evgeni.
==========
List tasks with active Assignee:
----------
ID: 1, Title: BigBugBigBugBigBug
Description: from time to time
Priority: HIGH, Assignee: Atanas
Severity: MAJOR, Status: FIXED
Steps to reproduce:
1. start program.
2. go to properties.
3. here it is.
----------
----------
Comments:
Content: nothing is working, Author: Borislav
----------
----------
History:
Activity: Bug with id:1 created., time: 2022-12-30 20:34:35
Activity: Task with id: 1 - assigned to Borislav., time: 2022-12-30 20:34:35
Activity: Comment added to task: 1 by: Borislav., time: 2022-12-30 20:34:35
Activity: Assignee Borislav was unassigned by Borislav., time: 2022-12-30 20:34:35
Activity: Task with id: 1 - assigned to Atanas., time: 2022-12-30 20:34:35
----------
----------
ID: 2, Title: TestCreateStory
Description: Great Story
Priority: HIGH, Assignee: Atanas
----------
----------
#No Comments#
----------
----------
History:
Activity: Story with id:2 created., time: 2022-12-30 20:34:35
Activity: Task with id: 2 - assigned to Atanas., time: 2022-12-30 20:34:35
----------
----------
ID: 4, Title: BigBugBigBugBigBug
Description: from time to time
Priority: HIGH, Assignee: Evgeni
Severity: MAJOR, Status: FIXED
Steps to reproduce:
1. start program.
2. go to properties.
3. here it is.
----------
----------
Comments:
Content: bug solved, Author: Evgeni
----------
----------
History:
Activity: Bug with id:4 created., time: 2022-12-30 20:34:35
Activity: Task with id: 4 - assigned to Evgeni., time: 2022-12-30 20:34:35
Activity: Comment added to task: 4 by: Evgeni., time: 2022-12-30 20:34:36
----------
==========
==========
Show team activity: Buda123
----------
History:
Activity: Bug with id:1 created., time: 2022-12-30 20:34:35
Activity: Task with id: 1 - assigned to Borislav., time: 2022-12-30 20:34:35
Activity: Comment added to task: 1 by: Borislav., time: 2022-12-30 20:34:35
Activity: Assignee Borislav was unassigned by Borislav., time: 2022-12-30 20:34:35
Activity: Task with id: 1 - assigned to Atanas., time: 2022-12-30 20:34:35
Activity: Story with id:2 created., time: 2022-12-30 20:34:35
Activity: Task with id: 2 - assigned to Atanas., time: 2022-12-30 20:34:35
Activity: Feedback with id:3 created., time: 2022-12-30 20:34:35
----------
----------
#No History#
----------
==========
==========
Show person activity: Atanas
----------
Content: Person with name:Atanas created. Date: 2022-12-30 20:34:35
Content: Joined team with name: Buda123 Date: 2022-12-30 20:34:35
Content: Assigned task with id:1. Date: 2022-12-30 20:34:35
Content: Assigned task with id:2. Date: 2022-12-30 20:34:35
----------
==========
Expected: 2, Received: 1. Invalid number of arguments!
==========
Show activity of board: Autobahn from team: Buda123
----------
Content: Bug with id:1 created. Date: 2022-12-30 20:34:35
Content: Task with id: 1 - assigned to Borislav. Date: 2022-12-30 20:34:35
Content: Comment added to task: 1 by: Borislav. Date: 2022-12-30 20:34:35
Content: Assignee Borislav was unassigned by Borislav. Date: 2022-12-30 20:34:35
Content: Task with id: 1 - assigned to Atanas. Date: 2022-12-30 20:34:35
Content: Story with id:2 created. Date: 2022-12-30 20:34:35
Content: Task with id: 2 - assigned to Atanas. Date: 2022-12-30 20:34:35
Content: Feedback with id:3 created. Date: 2022-12-30 20:34:35
----------
==========
==========
List All Tasks :
----------
ID: 1, Title: BigBugBigBugBigBug
Description: from time to time
Priority: HIGH, Assignee: Atanas
Severity: MAJOR, Status: FIXED
Steps to reproduce:
1. start program.
2. go to properties.
3. here it is.
----------
----------
ID: 4, Title: BigBugBigBugBigBug
Description: from time to time
Priority: HIGH, Assignee: Evgeni
Severity: MAJOR, Status: FIXED
Steps to reproduce:
1. start program.
2. go to properties.
3. here it is.
----------
----------
ID: 2, Title: TestCreateStory
Description: Great Story
Priority: HIGH, Assignee: Atanas
----------
----------
ID: 3, Title: TestFeedback
Description: It will be strange if works.
Rating: 1, Status: SCHEDULED
----------
==========
